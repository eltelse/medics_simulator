﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Net.Sockets;
using Advantech.Adam;
using Newtonsoft.Json;
using System.IO;
using System.Timers;


namespace simulator
{
     /// <summary>
     /// adamcontrol class manages all ADAM actions
     /// </summary>
    public class adamControl
    {
        public bool m_bStart;
        public AdamSocket adamModbus;
        public Adam6000Type m_Adam6000Type;
        public string m_szIP;
        public int m_iPort;
        public int m_iDoTotal, m_iDiTotal, m_iCount;
        public int m_iTimeout;
        public List<string> adamMap;
        public bool connected = false;
        public bool startFlag;
        public System.Timers.Timer m_i_PingTimer;
        public adamControl()
        {
            string path = Path.Combine(Environment.CurrentDirectory, "adamDetails.json");
            var adamDetails = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(@path));   // get adam details from json file
            int port = Int32.Parse(adamDetails["port"]);
            m_iTimeout = Int32.Parse(adamDetails["timeout"]);
            string ip = adamDetails["IP"];
            startFlag = Boolean.Parse(adamDetails["startFlag"]);
            adamMap = new List<string>();
            for(int i=0;i<adamDetails.Count;i++)
            {
                try
                {
                    string index = string.Format("D{0}", i);
                    string value = adamDetails[index];
                    adamMap.Add(value);
                }
                catch
                {
                    break;
                }
                
            }
            if(connect(ip, port, startFlag))
            {
                connected = true;
                startPingTImer();
            }
        }

        private void startPingTImer()
        {
            m_i_PingTimer = new System.Timers.Timer(m_iTimeout);
            m_i_PingTimer.Elapsed += OnTimedEvent;
            m_i_PingTimer.Start();
        }
		
		private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            makeAction(14 + 17, 0);
        }
        /// <summary>
        /// get element name and return its ADAM index
        /// </summary>
        /// <param name="name"></param>
        /// <returns>ADAM index</returns>
        public int getIndexByName(string name)
        {
           for(int i=0;i<adamMap.Count;i++)
            {
                if(adamMap[i]==name)
                {
                    return 17+i;
                }
            }
            return -1;
        }
        /// <summary>
        /// create connection to ADAM
        /// </summary>
        /// <param name="IP"></param>
        /// <param name="port"></param>
        /// <param name="startFlag"></param>
        /// <returns>true if connection created successfully, false otherwise</returns>
        public bool connect(string IP, int port, Boolean startFlag)
        {
            m_bStart = startFlag;			// the action stops at the beginning
            m_szIP = IP;	// modbus slave IP address
            m_iPort = port;				// modbus TCP port is 502
            adamModbus = new AdamSocket();
            adamModbus.SetTimeout(1000, 1000, 1000); // set timeout for TCP
            m_Adam6000Type = Adam6000Type.Adam6250; // the sample is for ADAM-6250
            return adamModbus.Connect(m_szIP, ProtocolType.Tcp, m_iPort);
        }

        /// <summary>
        /// make ADAM action
        /// </summary>
        /// <param name="index" ADAM index to make action on></param>
        /// <param name="IOflag" action value></param>
        /// <returns>true if action succeeded, false otherwise</returns>
        public bool makeAction(int index,int IOflag)
        {
            /*if (adamModbus.Modbus().ForceSingleCoil(index, IOflag))
            {
                return true;
            }
            else
            {
                //MessageBox.Show("ישנה תקלת חיבור לאמצעים", "שגיאה", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }*/
            bool x = adamModbus.Modbus().ForceSingleCoil(index, IOflag);
            return true;
        }
        /// <summary>
        /// reset ADAM, turn off all bits
        /// </summary>
        public void reset()
        {
            for(int i=0;i<15;i++)
            {
                makeAction(i + 17, 0);
            }
        }

    }
}
