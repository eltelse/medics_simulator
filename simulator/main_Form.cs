﻿using AudioSwitcher.AudioApi.CoreAudio;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace simulator
{
    public partial class main_Form : Form
    {
        Dictionary<String,int> controls_types_amount = new Dictionary<string, int> { { "effect sound",1}, { "background sound", 1 } };
        public utils myUtils;
        public Dictionary<String, String> controls_type_presentation_name;
        public scenario current_scenario = null;
        public Dictionary<String,Label> labels;
        public List<ComboBox> identifiers_CB;
        public Boolean acvtive_scenario=false;
        public Boolean chose_background_sound = false;
        public Boolean chose_effect_sound = false;
        public Dictionary<String, FileInfo> background_sounds = new Dictionary<string, FileInfo>();
        public Dictionary<String, FileInfo> effect_sounds = new Dictionary<string, FileInfo>();
        public audioPlayer background_sound_player;
        public audioPlayer effect_sound_player;
        private double backraoud_sound_current_time=0;
        private double effect_sound_current_time=0;
        public adamControl adam_action;
        public bool forceClose = false;
        public static Dictionary<String, Action_Button> elements;
        
       
        public main_Form()
        {
            InitializeComponent();
            myUtils = new utils();
            controls_type_presentation_name = new Dictionary<String, String> { { "smoke", "עשן" }, { "light", "תאורה" }, { "flicker", "פליקר" }, { "chaka", "צ'קלקה" }, {"עשן", "smoke"}, { "תאורה" , "light"}, {  "פליקר" ,"flicker"}, {  "צ'קלקה", "chaka" },{ "סאונד אפקט","effect sound"},{"סאונד רקע","background effect" } };
            load_audio_files_to_lists();
            identifiers_CB = new List<ComboBox> {identifier_CB1,identifier_CB2,identifier_CB3,identifier_CB4};
        }



        /// <summary>
        /// load mp3 files from audio directories 
        /// </summary>
        public void load_audio_files_to_lists()
        {
            String backround_files_path = utils.myPaths.paths["BACKGROUND_SOUND_AUDIO_FILES"];
            String effect_files_path = utils.myPaths.paths["EFFECT_SOUND_AUDIO_FILES"];
            DirectoryInfo back_dir = new DirectoryInfo(backround_files_path);
            DirectoryInfo effect_dir = new DirectoryInfo(effect_files_path);
            FileInfo[] back_Files = back_dir.GetFiles("*.mp3");
            FileInfo[] effect_Files = effect_dir.GetFiles("*.mp3"); 
            foreach (FileInfo file in back_Files)
            {
                background_sounds.Add(file.Name.Substring(0,file.Name.IndexOf('.')),file);
            }
            foreach (FileInfo file in effect_Files)
            {
                effect_sounds.Add(file.Name.Substring(0, file.Name.IndexOf('.')), file);
            }

        }
        public void Form1_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            this.WindowState = FormWindowState.Maximized;
            adam_action = new adamControl();
            adam_action.reset();
            foreach (DataGridViewColumn col in scenarioTable.Columns)
            {
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            fill_action_bt_list();
            load_Checkboxes();
            trackBar2.Value = trackBar2.Maximum/2;
            utils.currentVol = trackBar2.Value*10;
            foreach (Control c in room_map_panel.Controls)
            {
                if (c is Action_Button)
                {
                    ((Action_Button)c).main = this;
                }
               
            }
        }
        private void fill_action_bt_list()
        {
            elements = new Dictionary<string, Action_Button>();
            foreach(Control c in room_map_panel.Controls)
            {
                if(c is Action_Button)
                {
                    elements.Add(c.Name, (Action_Button)c);
                }
            }
        }

        private void load_Checkboxes()
        {
            foreach(Control c in room_map_panel.Controls)
            {
                if(c is Action_Button)
                {
                    String type = c.Name.Remove(c.Name.LastIndexOf('_'));
                    if(controls_types_amount.Keys.Contains(type))
                    {
                        controls_types_amount[type] += 1;
                    }
                    else
                    {
                        controls_types_amount[type] = 1;

                    }
                }
            }
            foreach (Control c2 in panel7.Controls)
            {
                if (c2 is ComboBox  && c2.Name.Contains("type"))
                {
                   foreach (String s in controls_types_amount.Keys)
                   {
                        if(s != "chaka" && s != "effect sound" && s!= "background sound")
                            ((ComboBox)c2).Items.Add(controls_type_presentation_name[s]);
                   }
        
                }
            }
        }

       

        private void type_CB1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (type_CB1.SelectedIndex != -1)
            {
                String selectedItem = type_CB1.Items[type_CB1.SelectedIndex].ToString();
                if (selectedItem != null)
                {
                    fit_identifeir_after_type_selection(0, selectedItem);
                }
            }
        }

        /// <summary>
        /// in scenario creation tab, after picking an element this function sets the next combobox to contain the right values 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="selectedItem"></param>
        private void fit_identifeir_after_type_selection(int index,String selectedItem)
        {
            
            identifiers_CB[index].Items.Clear();
            identifiers_CB[index].ResetText();
            identifiers_CB[index].SelectedIndex = -1;
            
            if (selectedItem == "סאונד אפקט")
            {
                foreach (String s in effect_sounds.Keys)
                {
                    identifiers_CB[index].Items.Add(s);
                }
            }
            else if (selectedItem == "סאונד רקע")
            {
                foreach (String s in background_sounds.Keys)
                {
                    identifiers_CB[index].Items.Add(s);
                }
            }
            else
            {
                int controls_amount = controls_types_amount[controls_type_presentation_name[selectedItem]];
                for (int i = 1; i <= controls_amount; i++)
                {
                    identifiers_CB[index].Items.Add(i.ToString());
                }
            }
        }


        private void pictureBox10_Click(object sender, EventArgs e)
        {
            if(trackBar2.Value<=8)
            {
                trackBar2.Value += 2;
                int newVol = trackBar2.Value * 10;
                setVolume(newVol);
            }
                
        }
  
        private void pictureBox9_Click(object sender, EventArgs e)
        {
            if(trackBar2.Value>=2)
            {
                trackBar2.Value -= 2;
                int newVol = trackBar2.Value * 10;
                setVolume(newVol);
            }



        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            audio_file_selection afs = new audio_file_selection(this,utils.myPaths.paths["BACKGROUND_SOUND_AUDIO_FILES"],1);
            afs.ShowDialog();
            afs.BringToFront();
        }

        private void label11_Click(object sender, EventArgs e)
        {
            audio_file_selection afs = new audio_file_selection(this, utils.myPaths.paths["BACKGROUND_SOUND_AUDIO_FILES"],1);
            afs.ShowDialog();
            afs.BringToFront();
        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            audio_file_selection afs = new audio_file_selection(this, utils.myPaths.paths["EFFECT_SOUND_AUDIO_FILES"],2);
            afs.ShowDialog();
            afs.BringToFront();
        }

        private void label14_Click(object sender, EventArgs e)
        {
            audio_file_selection afs = new audio_file_selection(this, utils.myPaths.paths["EFFECT_SOUND_AUDIO_FILES"],2);
            afs.ShowDialog();
            afs.BringToFront();
        }

        private void pictureBox20_Click(object sender, EventArgs e)
        {
            audio_file_selection afs = new audio_file_selection(this,utils.myPaths.paths["SCENARIO_FILES"]);
            afs.ShowDialog();
            afs.BringToFront();
        }

       
        
        private void background_sound_play_BT_Click(object sender, EventArgs e)
        {
            
            if(!chose_background_sound)
            {
                MessageBox.Show( "אנא בחר קובץ להשמיע", "שגיאה", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(!background_sound_player.is_palying) //play sound file
            {
                background_sound_play_BT.Image = Properties.Resources.pause_Button;
                background_sound_Timer.Start();
                background_sound_player.is_palying = true;
                this.playing_background_file_LB.Text = background_sound_player.name;
                background_sound_player.play();
            }
            else // pause sound file
            {
                background_sound_play_BT.Image = Properties.Resources.play_Button;
                background_sound_Timer.Stop();
                background_sound_player.is_palying = false;
                this.playing_background_file_LB.Text = background_sound_player.name;
                background_sound_player.pause();
           
            }
        }

        private void background_sound_Timer_Tick(object sender, EventArgs e)
        {
            String ticked = make_a_tick(backgorung_sound_time_LB.Text);
            if(ticked=="")
            {
                background_sound_Timer.Stop();
                this.current_scenario.stop();
                backgorung_sound_time_LB.Text = "00:00/" + current_scenario.current_backgorund_sound.length;
                background_sound_play_BT.Image = Properties.Resources.play_Button;
                return;
            }
            backgorung_sound_time_LB.Text = ticked;
            
        }
        /// <summary>
        /// gets a text in "00:00/00:00" format and makes a "time tick" if time is up return empty string, otherwise time label after tick
        /// </summary>
        /// <param name="time_as_text"></param>
        /// <returns>time after tick string</returns>
        private String make_a_tick(String time_as_text)
        {
            String currentTime = time_as_text.Split('/')[0];
            String totalTime = time_as_text.Split('/')[1];
            if(currentTime==totalTime)
            {
                return "";
            }
            int minutes = Int32.Parse(currentTime.Split(':')[0]);
            int seconds = Int32.Parse(currentTime.Split(':')[1]);
            
            if(seconds==59)
            {
                seconds = 0;
                minutes += 1;
            }
            else
            {
                seconds += 1;
            }
            String min_Str = minutes.ToString();
            String sec_Str = seconds.ToString();
            if (minutes < 10)
            {
                min_Str = "0" + min_Str;
            }
            if (seconds < 10)
            {
                sec_Str = 0 + sec_Str;
            }
            return  String.Format("{0}:{1}/{2}",min_Str, sec_Str, totalTime);
        }

        private void effect_sound_play_BT_Click(object sender, EventArgs e)
        {
            
            if (!chose_effect_sound)
            {
                MessageBox.Show("אנא בחר קובץ להשמיע", "שגיאה", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(!effect_sound_player.is_palying)
            {
                effect_sound_play_BT.Image = Properties.Resources.pause_Button;
                effect_sound_Timer.Start();
                effect_sound_player.is_palying = true;
                this.playing_effect_file_LB.Text = effect_sound_player.name;
                effect_sound_player.play();
               
            }
            else
            {
                effect_sound_play_BT.Image = Properties.Resources.play_Button;
                effect_sound_Timer.Stop();
                effect_sound_player.is_palying = false;
                effect_sound_player.pause();
            }
            if(effect_sound_player!=null)
            {
                this.playing_effect_file_LB.Text = effect_sound_player.name;

            }
        }

        private void effect_sound_Timer_Tick(object sender, EventArgs e)
        {
            String ticked = make_a_tick(effect_sound_time_LB.Text);
            if (ticked == "")
            {
                effect_sound_Timer.Stop();
                this.current_scenario.stop();
                effect_sound_time_LB.Text = "00:00/" + current_scenario.current_effect_sound.length;
                effect_sound_play_BT.Image = Properties.Resources.play_Button;
                return;
            }
            effect_sound_time_LB.Text = ticked;
        }

        private void background_sound_stop_BT_Click(object sender, EventArgs e)
        {
            if (chose_background_sound)
            {
                background_sound_Timer.Dispose();
                playing_background_file_LB.Text = "קובץ לא הופעל";
                backgorung_sound_time_LB.Text = "00:00/" + background_sound_player.length;
                background_sound_play_BT.Image = Properties.Resources.play_Button;
                backraoud_sound_current_time = 0;
                background_sound_player.stop();
            }
        }

        private void effect_sound_stop_BT_Click(object sender, EventArgs e)
        {
            if (chose_effect_sound)
            {
                effect_sound_Timer.Dispose();
                playing_effect_file_LB.Text = "קובץ לא הופעל";
                effect_sound_time_LB.Text = "00:00/" + effect_sound_player.length;
                effect_sound_play_BT.Image = Properties.Resources.play_Button;
                effect_sound_current_time = 0;
                effect_sound_player.stop();
            }
            
            
        }

        private void type_CB2_SelectedValueChanged(object sender, EventArgs e)
        {
            if (type_CB2.SelectedIndex != -1)
            {
                String selectedItem = type_CB2.Items[type_CB2.SelectedIndex].ToString();
                if (selectedItem != null)
                {
                    fit_identifeir_after_type_selection(1, selectedItem);
                }
            }
        }

        private void type_CB3_SelectedValueChanged(object sender, EventArgs e)
        {
            if (type_CB3.SelectedIndex != -1)
            {
                String selectedItem = type_CB3.Items[type_CB3.SelectedIndex].ToString();
                if (selectedItem != null)
                {
                    fit_identifeir_after_type_selection(2, selectedItem);
                }
            }
        }

        private void type_CB4_SelectedValueChanged(object sender, EventArgs e)
        {
            if (type_CB4.SelectedIndex != -1)
            {
                String selectedItem = type_CB4.Items[type_CB4.SelectedIndex].ToString();
                if (selectedItem != null)
                {
                    fit_identifeir_after_type_selection(3, selectedItem);
                }
            }
        }
        private void maskedTextBox1_Click(object sender, EventArgs e)
        {
            maskedTextBox1.Select(0,1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[] bitarr = { 0, 0, 0, 0 };
            bitarr[0] = inputvalidation(type_CB1, identifier_CB1, maskedTextBox1, maskedTextBox5); 
            bitarr[1] = inputvalidation(type_CB2, identifier_CB2, maskedTextBox2, maskedTextBox6);
            bitarr[2] = inputvalidation(type_CB3, identifier_CB3, maskedTextBox3, maskedTextBox7);
            bitarr[3] = inputvalidation(type_CB4, identifier_CB4, maskedTextBox4, maskedTextBox8);
            
            for(int i=0;i<4;i++)
            {
                if(bitarr[i]==-1)
                {
                    return;
                }
            }
            if(bitarr[0]>0)
            {
                scenarioTable.Rows.Add(type_CB1.SelectedItem.ToString() + " " + identifier_CB1.SelectedItem.ToString(), maskedTextBox1.Text, fix_time_formating(maskedTextBox5.Text));

            }
            if (bitarr[1] > 0)
            {
                scenarioTable.Rows.Add(type_CB2.SelectedItem.ToString() + " " + identifier_CB2.SelectedItem.ToString(), maskedTextBox2.Text, maskedTextBox6.Text);

            }
            if (bitarr[2] > 0)
            {
                scenarioTable.Rows.Add(type_CB3.SelectedItem.ToString() + " " + identifier_CB3.SelectedItem.ToString(), maskedTextBox3.Text, maskedTextBox7.Text);

            }
            if (bitarr[3] > 0)
            {
                scenarioTable.Rows.Add(type_CB4.SelectedItem.ToString() + " " + identifier_CB4.SelectedItem.ToString(), maskedTextBox4.Text, maskedTextBox8.Text);

            }

            scenarioTable.Sort(scenarioTable.Columns[1],ListSortDirection.Ascending);
            cleanFields(false);
        }
        /// <summary>
        /// clean all elements in scenario creation tab
        /// </summary>
        /// <param name="clearName"></param>
        private void cleanFields(bool clearName)
        {
            foreach (Control c in panel7.Controls)
            {
                if (c is ComboBox)
                {
                    ((ComboBox)c).SelectedIndex = -1;
                }
                if (c is MaskedTextBox)
                {
                    ((MaskedTextBox)c).Clear();
                }
                if (c is TextBox)
                {
                    if(clearName)
                        ((TextBox)c).Text = "";
                }
                if(c is CheckBox)
                {
                    if(clearName)
                        ((CheckBox)c).Checked = false;
                }
            }
        }
        /// <summary>
        /// check input validation in all scenario creation elements
        /// </summary>
        /// <param name="element"></param>
        /// <param name="type"></param>
        /// <param name="startTime"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        private int inputvalidation(ComboBox element, ComboBox type, MaskedTextBox startTime , MaskedTextBox duration)
        {
            int counter = 0;
            int flag = 0;
            for(int i=0;i<2;i++)
            {
                if (element.SelectedIndex != -1)
                {
                    counter++;
                    flag = 1;
                }
                if (type.SelectedIndex == -1 && counter > 0)
                {
                    type.BackColor = Color.LightCoral;
                    flag = -1;
                    
                }
                if (type.SelectedIndex != -1)
                {
                    counter++;
                }
                if ((startTime.Text == "  :" && counter > 0) || startTime.Text == "00:00")
                {
                    startTime.BackColor = Color.LightCoral;
                    flag = -1;
                }
                else if(startTime.MaskCompleted)
                {
                    counter++;
                }
                if((duration.Text== "" || duration.Text=="0000") && counter>0)
                {
                    duration.BackColor = Color.LightCoral;
                    flag = -1;
                }
                else if(duration.MaskCompleted)
                {
                    counter++;
                }
            }
            return flag;
        }

        private String fix_time_formating(String s)
        {
           
           
                if (s.Length == 3)
                {
                    return s;
                }
                int numOfZeroes = 3 - s.Length;
                String tempStr = "";
                for (int i = 0; i < numOfZeroes; i++)
                {
                    tempStr += "0";
                }
                return tempStr + s;

        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            helpPopUp helpop = new helpPopUp(this);
            helpop.Show();
            
        }

  
        private void trackBar2_MouseCaptureChanged(object sender, EventArgs e)
        {
            int newVol = trackBar2.Value *10;
            setVolume(newVol);
        }
        /// <summary>
        /// set unified volume to all audio players
        /// </summary>
        /// <param name="newVol"></param>
        private void setVolume(int newVol)
        {
            utils.currentVol = newVol;
            if (background_sound_player != null)
            {
                background_sound_player.setVol(newVol);
            }
            if (effect_sound_player != null)
            {
                effect_sound_player.setVol(newVol);
            }
            if (current_scenario != null)
            {
                if (current_scenario.current_effect_sound != null)
                {
                    current_scenario.current_effect_sound.setVol(newVol);
                }
                if (current_scenario.current_backgorund_sound != null)
                {
                    current_scenario.current_backgorund_sound.setVol(newVol);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(scenarioTable.Rows.Count==0 && !chakaStartCB.Checked && !chackEndCB.Checked)
            {
                MessageBox.Show("התרחיש לא מכיל פעולות, הוסף פעולות ואז שמור את התרחיש", "שגיאה", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if(checkNameExistence(scenarioNameTB.Text))
            {
                MessageBox.Show("שם קובץ כבר קיים, בחר שם אחר", "שגיאה", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                scenario newScenario = new scenario(scenarioNameTB.Text);
                if (chakaStartCB.Checked)
                {
                    Action chakaAction = new Action("chaka_1", 1, true);
                    newScenario.Actions.Add(chakaAction);
                }
                foreach (DataGridViewRow row in scenarioTable.Rows)
                {
                    string chosenElement = row.Cells["element"].Value.ToString();
                    string element;
                    string chosenSound;

                    if (chosenElement.Contains("סאונד אפקט"))
                    {
                        chosenSound = chosenElement.Substring("סאונד אפקט ".Length);
                        element = "effect_sound" + " " + chosenSound;
                    }
                    else if (chosenElement.Contains("סאונד רקע"))
                    {
                        chosenSound = chosenElement.Substring("סאונד רקע ".Length);
                        element = "background_sound" + " " + chosenSound;

                    }
                    else
                    {
                        String elementType = chosenElement.Split(' ')[0];
                        String elementnumber = chosenElement.Split(' ')[1];
                        element = controls_type_presentation_name[elementType] + "_" + elementnumber;
                    }
                    int startTime = utils.timeToSeconds(row.Cells["startTime"].Value.ToString());
                    int durance = Int32.Parse(row.Cells["durance"].Value.ToString());
                    newScenario.addAction(element, startTime, durance);
                }
                if (chakaStartCB.Checked)
                {
                    int lastActionTime = newScenario.Actions[newScenario.Actions.Count - 1].time;
                    Action chakaAction = new Action("chaka_1", lastActionTime + 2, false);
                    newScenario.Actions.Add(chakaAction);

                }
                utils.addScenarioToJson(newScenario);
                MessageBox.Show("(: תרחיש נוסף בהצלחה ", "מעולה!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cleanFields(true);
                scenarioTable.Rows.Clear();
            }
        }
        /// <summary>
        /// reset all action Buttons and ADAM indexes
        /// </summary>
        public void resetElements()
        {
            foreach(Control ab in room_map_panel.Controls)
            {
                if(ab is Action_Button && ((Action_Button)ab).status)
                {
                    ((Action_Button)ab).clicked();
                }
            }
        }

        private bool checkNameExistence(string name)
        {
            foreach(scenario sc in utils.scenarios)
            {
                if (sc.scenarioName == name)
                    return true;
            }
            return false;
        }

        private void scenario_play_BT_Click(object sender, EventArgs e)
        {
            
            if (current_scenario==null)
            {
                MessageBox.Show("אנא בחר קובץ תרחיש", "שגיאה", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
            else if (!current_scenario.timer.Enabled) //play scenario
            {
                if(!current_scenario.paused)
                {
                    if (background_sound_player != null && background_sound_player.is_palying)
                    {
                        background_sound_player.stop();
                    }
                    if (effect_sound_player != null && effect_sound_player.is_palying)
                    {
                        effect_sound_player.stop();
                    }
                    resetElements();
                }
                scenario_play_BT.Image = Properties.Resources.pause_Button;
                current_scenario.start();
                panel4.Enabled = false;
                panel3.Enabled = false;
                scenarioTimer.Start();
                playing_scenario_name_LB.Text = current_scenario.scenarioName;
            }
            else // pause scenario
            {
                scenario_play_BT.Image = Properties.Resources.play_Button;
                current_scenario.pause();
            }
        }


        public void stopScenario()
        {
            scenarioTimer.Stop();
            scenario_play_BT.Image = Properties.Resources.play_Button;
            scenarioTimer.Dispose();
            scenario_timeLB.Text = String.Format("00:00/{0}", utils.secondsToTimeFormat(current_scenario.getTotalTime()));
            panel4.Enabled = true;
            panel3.Enabled = true;
        }

        private void scenario_stop_BT_Click(object sender, EventArgs e)
        {
            current_scenario.stop();
            resetElements();
            stopScenario();
            playing_scenario_name_LB.Text = "קובץ לא הופעל";
            scenarioTimer.Dispose();
            scenario_timeLB.Text = String.Format("00:00/{0}",utils.secondsToTimeFormat(current_scenario.getTotalTime()));
        }

        private void scenarioTimer_Tick(object sender, EventArgs e)
        {
            String totalTime = scenario_timeLB.Text.Split('/')[1];
            String currentTime = utils.secondsToTimeFormat(current_scenario.secondsFromstart);
            scenario_timeLB.Text = String.Format("{0}/{1}", currentTime, totalTime);
            if(current_scenario.secondsFromstart>=current_scenario.Actions[current_scenario.Actions.Count-1].time)
            {
                stopScenario();
            }
            
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            utils.scenarios.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(chaka_1.clicked()) //turn chaka on
            {
                button4.BackColor = Color.Green;
                button4.ForeColor = Color.White;
            }
            else
            {
                button4.BackColor = Color.Gainsboro;
                button4.ForeColor = Color.Black;
            }
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void maskedTextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if(maskedTextBox1.MaskFull)
            {
                maskedTextBox1.BackColor = Color.White;
            }
            if (!maskedTextBox1.MaskFull)
            {
                maskedTextBox1.BackColor = Color.LightCoral;
            }
        }

        private void maskedTextBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if (maskedTextBox2.MaskFull)
            {
                maskedTextBox2.BackColor = Color.White;
            }
            if (!maskedTextBox2.MaskFull)
            {
                maskedTextBox2.BackColor = Color.LightCoral;
            }
        }

        private void maskedTextBox3_KeyUp(object sender, KeyEventArgs e)
        {
            if (maskedTextBox3.MaskFull)
            {
                maskedTextBox3.BackColor = Color.White;
            }
            if (!maskedTextBox3.MaskFull)
            {
                maskedTextBox3.BackColor = Color.LightCoral;
            }
        }

        private void maskedTextBox4_KeyUp(object sender, KeyEventArgs e)
        {
            if (maskedTextBox4.MaskFull)
            {
                maskedTextBox4.BackColor = Color.White;
            }
            if (!maskedTextBox4.MaskFull)
            {
                maskedTextBox4.BackColor = Color.LightCoral;
            }
        }

        private void maskedTextBox5_KeyUp(object sender, KeyEventArgs e)
        {
            if (maskedTextBox5.Text != "")
            {
                maskedTextBox5.BackColor = Color.White;
            }
        }

        private void maskedTextBox6_KeyUp(object sender, KeyEventArgs e)
        {
            if (maskedTextBox6.Text != "")
            {
                maskedTextBox6.BackColor = Color.White;
            }
        }

        private void maskedTextBox7_KeyUp(object sender, KeyEventArgs e)
        {
            if (maskedTextBox7.Text != "")
            {
                maskedTextBox7.BackColor = Color.White;
            }
        }

        private void maskedTextBox8_KeyUp(object sender, KeyEventArgs e)
        {
            if (maskedTextBox8.Text != "")
            {
                maskedTextBox8.BackColor = Color.White;
            }
        }

        private void identifier_CB1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(identifier_CB1.SelectedIndex!=-1)
            {
                identifier_CB1.BackColor = Color.White;
            }
        }

        private void identifier_CB2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (identifier_CB2.SelectedIndex != -1)
            {
                identifier_CB2.BackColor = Color.White;
            }
        }

        private void identifier_CB3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (identifier_CB3.SelectedIndex != -1)
            {
                identifier_CB3.BackColor = Color.White;
            }
        }

        private void identifier_CB4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (identifier_CB4.SelectedIndex != -1)
            {
                identifier_CB4.BackColor = Color.White;
            }
        }

        private void tabControl2_Selecting(object sender, TabControlCancelEventArgs e)
        {
           
                if (tabControl2.SelectedTab == tabControl2.TabPages["activationTab"])
                {
                    if (fieldsNotEmpty())
                    {
                        DialogResult d = MessageBox.Show(" האם ברצונך לצאת ממסך עריכת תרחיש? נתונים שלא נשמרו ימחקו", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (d == DialogResult.Yes)
                        {
                            cleanFields(true);
                            scenarioTable.Rows.Clear();

                        }

                        else if (d == DialogResult.No)
                        {
                            tabControl2.SelectedTab = tabControl2.TabPages["creationTab"];
                        }
                    }
                }
            
           
        }
        /// <summary>
        /// check existence of not empty field in scenario creation tab
        /// </summary>
        /// <returns>true if not empty field found, false otherwise</returns>
        private bool fieldsNotEmpty()
        {
            foreach (Control c in panel7.Controls)
            {
                if (c is ComboBox)
                {
                    if(((ComboBox)c).SelectedIndex != -1)
                    {
                        return true;
                    }
                    
                }
                if (c is MaskedTextBox)
                {
                    if(((MaskedTextBox)c).MaskCompleted)
                    {
                        return true;
                    }
                }
            }
            if(scenarioTable.Rows.Count>0)
            {
                return true;
            }
            return false;
        }

        private void main_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!forceClose)
            {
                if (tabControl2.SelectedTab == tabControl2.TabPages["creationTab"] && fieldsNotEmpty())  // some detaisl will not be saved
                {
                    DialogResult d = MessageBox.Show("האם ברצונך לסגור את האפליקציה? נתוני תרחיש שלא נשמרו יימחקו", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (d == DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                    if (d == DialogResult.Yes)
                    {
                        adam_action.reset();
                        System.Environment.Exit(1);
                    }
                }
                else // nothing to save
                {
                    DialogResult d = MessageBox.Show(" האם ברצונך לסגור את האפליקציה?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (d == DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                    if (d == DialogResult.Yes)
                    {
                        adam_action.reset();
                        System.Environment.Exit(1);
                    }
                }
            }
            
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            tabControl2.Location = new Point(tabControl2.Location.X , tabControl2.Location.Y + 5);

        }

        private void button3_Click_2(object sender, EventArgs e)
        {
            tabControl2.Location = new Point(tabControl2.Location.X - 5, tabControl2.Location.Y);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            tabControl2.Location = new Point(tabControl2.Location.X + 5, tabControl2.Location.Y);

        }

        private void button10_Click(object sender, EventArgs e)
        {
            tabControl2.Location = new Point(tabControl2.Location.X, tabControl2.Location.Y - 5);

        }

        private void button8_Click(object sender, EventArgs e)
        {
            tabControl2.Width = Convert.ToInt32(tabControl2.Width* (1 + scale));
            setSize(true,room_map_panel);
            setSize(true,panel3);
            setSize(true,panel4);
            setSize(true,panel5);
        }
        double scale = 0.05;
        private void setSize(bool increaseFlag ,Panel p)
        {
           
            foreach(Control c in p.Controls)
            {   
                if(increaseFlag)
                {
                    c.Width = Convert.ToInt32(c.Width * (1 + scale));
                }
                else
                {
                    c.Width = Convert.ToInt32(c.Width * (1 - scale));
                }
                
                if (c is Panel)
                {
                    setSize(increaseFlag,(Panel)c);
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            // tabControl2.Width = Convert.ToInt32(tabControl2.Width * (1 - scale));
            tabControl2.Width -= 5;
            label7.Text = tabControl2.Width.ToString();
            //setSize(true, room_map_panel);
            //setSize(true, panel3);
            //setSize(true, panel4);
            //setSize(true, panel5);
        }

        private void scenario_timeLB_Click(object sender, EventArgs e)
        {

        }

        private void playing_effect_file_LB_Click(object sender, EventArgs e)
        {

        }

        private void label27_Click(object sender, EventArgs e)
        {

        }

        private void type_CB3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void chackEndCB_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void smoke_1_Load(object sender, EventArgs e)
        {

        }
    }
    }


