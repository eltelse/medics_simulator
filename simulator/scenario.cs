﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace simulator
{
    public class scenario
    {
        public audioPlayer current_backgorund_sound;
        public audioPlayer current_effect_sound;
        public String scenarioName;
        public Boolean active;
        public System.Timers.Timer timer;
        public List<Action> Actions;
        public int currentActionIndex = 0;
        public int secondsFromstart = 0;
        public main_Form main;
        public bool paused = false;
        
        public scenario(String name)
        {
            Actions = new List<Action>();
            timer = new System.Timers.Timer(1000);
            timer.Elapsed += OnTimedEvent;
            active = false;
            this.scenarioName = name;
        }

        /// <summary>
        /// add a new action to actions list
        /// </summary>
        /// <param name="element"></param>
        /// <param name="starTime"></param>
        /// <param name="durance"></param>
       public void addAction(String element, int starTime, int durance)
        {
                Action starAction = new Action(element, starTime, true);
                insertActionToSortedList(starAction);
                if (durance > 0)
                {
                    Action endAction = new Action(element, starTime + durance, false);
                    insertActionToSortedList(endAction);
                }
        }

      
        /// <summary>
        /// insert action to actions list in a sorted way
        /// </summary>
        /// <param name="act"></param>
        private void insertActionToSortedList(Action act)
        {
            for (int i = 0; i <= Actions.Count; i++)
            {
                if (i == Actions.Count)
                {
                    Actions.Add(act);
                    break;
                }
                if (Actions[i].time > act.time)
                {
                    Actions.Insert(i, act);
                    break;
                }
            }
        }
        /// <summary>
        /// start scenario
        /// </summary>
        public void start()
        {
            paused = false;
            if(current_effect_sound!= null && current_effect_sound.paused)
            {
                current_effect_sound.play();
            }
            if(current_backgorund_sound != null && current_backgorund_sound.paused)
            {
                current_backgorund_sound.play();
            }
            this.active = true;
            if(!timer.Enabled)
            {
                timer = new System.Timers.Timer(1000);
                timer.Elapsed += OnTimedEvent;
            }
            timer.Enabled = true;
            active = true;
            timer.Start();
            
        }

        public void setMnain(main_Form main)
        {
            this.main = main;
        }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            if(secondsFromstart>=this.Actions[Actions.Count-1].time)
            {
                String elementName = ((Action)this.Actions[currentActionIndex]).elements;
                main_Form.elements[elementName].clicked();
                stop();
                
                return;
            }
            while (currentActionIndex<this.Actions.Count&&((Action)this.Actions[currentActionIndex]).time == secondsFromstart)
            {
                String elementName = ((Action)this.Actions[currentActionIndex]).elements;
                int cutLength;
                string filename,path;
                switch(elementName)
                {
                    case string a when a.Contains( "effect_sound"):
                        cutLength = elementName.Split(' ')[0].Length + 1;
                        filename = elementName.Substring(cutLength);
                        path = utils.myPaths.paths["EFFECT_SOUND_AUDIO_FILES"] + "\\" + filename;
                        if (current_effect_sound != null && current_effect_sound.is_palying)
                            current_effect_sound.stop();
                        else
                        {
                            current_effect_sound = new audioPlayer(path);
                            current_effect_sound.play();
                        }
                        
                        break;
                    case string b when b.Contains("background_sound"):
                        cutLength = elementName.Split(' ')[0].Length + 1;
                        filename = elementName.Substring(cutLength);
                        path = utils.myPaths.paths["BACKGROUND_SOUND_AUDIO_FILES"] + "\\" + filename;
                        if (current_backgorund_sound != null && current_backgorund_sound.is_palying)
                            current_backgorund_sound.stop();
                            
                        else
                        {
                            current_backgorund_sound = new audioPlayer(path);
                            current_backgorund_sound.play();
                        }
                        break;
                    default:
                        main_Form.elements[elementName].clicked();
                        break;
                }
                currentActionIndex += 1;
            }
            secondsFromstart += 1;
        }
        /// <summary>
        /// stop scenario
        /// </summary>
        public void stop()
        {
            this.active = false;
            paused = false;
            currentActionIndex = 0;
            secondsFromstart = 0;
            timer.Dispose();
            timer.Enabled = false;
            
            if (current_backgorund_sound != null && current_backgorund_sound.is_palying)
            {
                current_backgorund_sound.stop();
            }
            if (current_effect_sound != null && current_effect_sound.is_palying)
            {
                current_effect_sound.stop();
            }
            
        }
        /// <summary>
        /// pause scenario
        /// </summary>
        public void pause()
        {
            paused = true;
            timer.Stop();
            main.scenarioTimer.Stop();
            if(current_backgorund_sound != null && current_backgorund_sound.is_palying)
            {
                current_backgorund_sound.pause();
            }
            if(current_effect_sound != null && current_effect_sound.is_palying)
            {
                current_effect_sound.pause();
            }
        }
        /// <summary>
        /// get scenario total durance
        /// </summary>
        /// <returns>total time in seconds</returns>
        public  int getTotalTime()
        {
            if(Actions[Actions.Count - 1].elements!="chaka_1")
                return Actions[Actions.Count - 1].time;
            else
            {
                return Actions[Actions.Count - 2].time +2;
            }
        }
    }
}
