﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace simulator
{
    public partial class noConnectionErrorcs : Form
    {
        main_Form main;
        public noConnectionErrorcs(main_Form main)
        {
            InitializeComponent();
            this.CenterToParent();
            this.main = main;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            main.forceClose = true;
            main.Close();
            this.Close();
            Application.ExitThread();
            //Environment.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(main.adam_action.connect(main.adam_action.m_szIP,main.adam_action.m_iPort,main.adam_action.startFlag))
            {
                main.Enabled = true;
                this.Close();
            }
        }
    }
}
