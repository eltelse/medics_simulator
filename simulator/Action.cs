﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace simulator
{
    public class Action
    {
        public String elements;
        public int time;
        public bool isStart;


        public Action(String element , int timeInSeconds, bool isStart)
        {
            this.elements = element;
            this.time = timeInSeconds;
            this.isStart = isStart;
        }

    }
}
