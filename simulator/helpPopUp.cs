﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace simulator
{
    public partial class helpPopUp : Form
    {
        main_Form main;
        public helpPopUp(main_Form main)
        {
            InitializeComponent();
            this.CenterToScreen();
            this.main = main;
        }
       public helpPopUp()
        {
            InitializeComponent();
            this.CenterToScreen();
            this.main = null;
        }

        private void helpPopUp_Load(object sender, EventArgs e)
        {
            if(main != null && main.tabControl2.SelectedTab == main.tabControl2.TabPages["creationTab"])
            {
                richTextBox1.Text = "מסך מאפשר ליצור תרחישים חדשים למאגר התרחישים של המערכת.";
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
