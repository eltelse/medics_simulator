﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace simulator
{
    public class utils
    {
        public static RES myPaths;
        public static List<scenario> scenarios;
        public static int currentVol;
        public utils()
        {
            loadJson();
            loadScenarios();
            currentVol = 0;
        }

        public class RES
        {
            public Dictionary<string, string> paths { set; get; }
        }
        /// <summary>
        /// load all system information from json files
        /// </summary>
        private void loadJson()
        {
            String mypath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            String path = mypath + @"\path.json";
            String json = File.ReadAllText(mypath + @"\path.json");
            myPaths = JsonConvert.DeserializeObject<RES>(json);
            foreach(string key in myPaths.paths.Keys.ToArray())
            {
                if (myPaths.paths[key] == "")
                {
                    if (key != "SCENARIO_FILES")
                    {
                        myPaths.paths[key] = mypath + @"\audio_files\" + key;
                    }
                    else
                    {
                        myPaths.paths[key] = mypath;
                        int x = 5;
                    }
                }
                    
                
            }
            
        }
        public static String secondsToTimeFormat(int sec)
        {
            int seconds = sec % 60;
            int minutes = sec / 60;
            String SecStr = (sec % 60).ToString();
            String minStr = (sec / 60).ToString();
            if (seconds < 10)
            {
                if (seconds == 0)
                {
                    SecStr = "00";
                }
                else
                {
                    SecStr = String.Format("0{0}", (seconds).ToString());

                }
            }
            if (minutes < 10)
            {
                if (minutes == 0)
                {
                    minStr = "00";
                }
                else
                {
                    minStr = (minutes).ToString();
                }
            }
            String newStr = String.Format("{0}:{1}", minStr, SecStr);
            return newStr;

        }
        /// <summary>
        /// load scenarios from json file
        /// </summary>
        public static void loadScenarios()
        {
            scenarios = new List<scenario>();
            string path = Path.Combine(myPaths.paths["SCENARIO_FILES"], "scenarios.json");
            var jsonScenarios = JsonConvert.DeserializeObject<List<scenario>>(File.ReadAllText(@path));
            foreach (scenario sc in jsonScenarios)
            {
                scenarios.Add(sc);
            }
        }

        public static int timeToSeconds(String time)
        {
            String minutes = time.Split(':')[0];
            String seconds = time.Split(':')[1];
           return Int32.Parse(seconds) + Int32.Parse(minutes) * 60;
            
        }


        public static void addScenarioToJson(scenario sc)
        {
            loadScenarios();
            scenarios.Add(sc);
            var jsonToOutput = JsonConvert.SerializeObject(scenarios, Formatting.Indented);
            string path = Path.Combine(myPaths.paths["SCENARIO_FILES"], "scenarios.json");
            File.WriteAllText(path, jsonToOutput);
        }

        public static String time_format(TimeSpan dur)
        {
            double minutes = Math.Round(dur.TotalSeconds / 60);
            double seconds = Math.Round(dur.TotalSeconds % 60);
            String minutesStr = minutes.ToString();
            String secondsStr = seconds.ToString();
            if (minutes < 10)
            {
                minutesStr = "0" + minutesStr;
            }
            if (seconds < 10)
            {
                secondsStr = "0" + secondsStr;
            }
            return String.Format("{0}:{1}", minutesStr, secondsStr);
        }
    }
}
