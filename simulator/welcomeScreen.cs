﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace simulator
{
    public partial class welcomeScreen : Form
    {
        public welcomeScreen()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            main_Form main = new main_Form();
           /* adamControl adam_action = new adamControl();
            if (!adam_action.connected)
            {
                MessageBox.Show("ישנה תקלת חיבור לאמצעים", "שגיאה", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                this.Hide();
                main.Show();
            }*/
            this.Hide();
            main.Show();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            helpPopUp helpop = new helpPopUp();
            helpop.Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void welcomeScreen_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            adamControl adam_action = new adamControl();
            //if (!adam_action.connected)
            //{
           //     MessageBox.Show("ישנה תקלת חיבור לאמצעים", "שגיאה", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            // this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Left, Screen.PrimaryScreen.WorkingArea.Top);
        }
    }
}
