﻿using NAudio.Wave;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Security;
using System.Windows.Forms;

namespace simulator
{
    public partial class audio_file_selection : Form
    {
        main_Form main;
        String files_path;
        int RBLocation = 10;
        int sound_type_index;
        bool scenarioMode = false;
        int numOfRadioButtons=0;
        FileInfo[] Files;
        public audio_file_selection(main_Form main,String files_path,int sound_type_index)
        {
            InitializeComponent();
            this.main = main;
            this.CenterToScreen();
            this.files_path = files_path;
            this.sound_type_index = sound_type_index;
        }

        public audio_file_selection(main_Form main, String filePath)
        {
            InitializeComponent();
            this.main = main;
            this.files_path = filePath;
            init_radio_buttons_for_scenario();
            scenarioMode = true;
            this.CenterToScreen();
        }

        private void audio_file_selection_Load(object sender, EventArgs e)
        {
            init_radio_buttons_for_audio();
        }
        /// <summary>
        /// initial radio buttons in window according to files in audio directorie
        /// </summary>
        private void init_radio_buttons_for_audio()
        {
            DirectoryInfo dir = new DirectoryInfo(files_path);
            Files = dir.GetFiles("*.mp3"); //Getting mp3 files
           panel2.AutoScrollMinSize = new Size(0, Files.Length * 47);
            foreach (FileInfo file in Files)
            {
                RadioButton tempRB = new RadioButton();
                Mp3FileReader reader = new Mp3FileReader(file.FullName);
                TimeSpan duration = reader.TotalTime;
                tempRB.Text = file.Name.Substring(0,file.Name.IndexOf('.'));
                tempRB.Font = label1.Font;
                tempRB.TextAlign = ContentAlignment.MiddleLeft;
                tempRB.RightToLeft = RightToLeft.Yes;
                tempRB.Anchor = AnchorStyles.Right;
                tempRB.Width = 230;
                tempRB.Height = 30;
                tempRB.Location =new Point(0 , RBLocation);
                tempRB.Anchor = AnchorStyles.Top;
                tempRB.Left = panel2.Width - tempRB.Width - 25;
                panel2.Controls.Add(tempRB);
                RBLocation += tempRB.Height + 15;

                String time = time_format(duration);
                Label timeLB = new Label();
                timeLB.Text = time;
                timeLB.Location = new Point(10, tempRB.Location.Y);
                timeLB.TextAlign = ContentAlignment.MiddleCenter;
                timeLB.Font = tempRB.Font;
                panel2.Controls.Add(timeLB);
            }

        }
        /// <summary>
        /// initial radio buttons in window according to files in scenrio directorie
        /// </summary>
        private void init_radio_buttons_for_scenario()
        {
            
            foreach (scenario sc in utils.scenarios)
            {
                RadioButton tempRB = new RadioButton();
                tempRB.Text = sc.scenarioName;
                tempRB.Font = label1.Font;
                tempRB.TextAlign = ContentAlignment.MiddleLeft;
                tempRB.RightToLeft = RightToLeft.Yes;
                tempRB.Anchor = AnchorStyles.Top;
                tempRB.Width = 230;
                tempRB.Height = 30;
                tempRB.Location = new Point( 0,RBLocation);
                tempRB.Left = panel2.Width - tempRB.Width - 25;
                panel2.Controls.Add(tempRB);
                RBLocation += tempRB.Height + 15;
                numOfRadioButtons++;
                panel2.AutoScrollMinSize = new Size(numOfRadioButtons, 1000);

                Label timeLB = new Label();
                int minutes = sc.getTotalTime() / 60;
                int seconds = sc.getTotalTime() % 60;
                timeLB.Text = minutes.ToString() + ":" + seconds.ToString();
                timeLB.Location = new Point(10,tempRB.Location.Y);
                timeLB.TextAlign = ContentAlignment.MiddleCenter;
                timeLB.Font = tempRB.Font;
                panel2.Controls.Add(timeLB);

            }
        }
        /// <summary>
        /// convert timespan to string
        /// </summary>
        /// <param name="dur"></param>
        /// <returns>timespan as string</returns>
        private String time_format(TimeSpan dur)
        {
            double minutes = Math.Round(dur.TotalSeconds / 60);
            double seconds = Math.Round(dur.TotalSeconds % 60);
            String minutesStr = minutes.ToString();
            String secondsStr = seconds.ToString();
            if(minutes<10)
            {
                minutesStr = "0" + minutesStr;
            }
            if(seconds<10)
            {
                secondsStr = "0" + secondsStr;
            }
            return String.Format("{0}:{1}", minutesStr, secondsStr);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(noSelectedOptions())
            {
                this.Hide();
            }
            else 
            {
                if (scenarioMode)
                {
                    String name = "";
                    foreach (Control c in panel2.Controls)
                    {
                        if (c is RadioButton && ((RadioButton)c).Checked)
                        {
                            name = ((RadioButton)c).Text;
                            break;
                        }
                    }
                    foreach (scenario sc in utils.scenarios)
                    {
                        if (sc.scenarioName == name)
                        {
                            main.current_scenario = sc;
                            main.current_scenario.setMnain(main);
                            break;
                        }
                    }
                    if (name != null)
                        main.chosen_scenario_LB.Text = name;
                    main.scenario_timeLB.Text = String.Format("00:00/{0}", utils.secondsToTimeFormat(main.current_scenario.getTotalTime()));
                }
                String fileName = "";
                foreach (Control c in panel2.Controls)
                {
                    if (c is RadioButton && ((RadioButton)c).Checked)
                    {
                        String[] presentationName = ((RadioButton)c).Text.Split(' ');
                        String name = "";
                        for(int i=0;i<presentationName.Length;i++)
                        {
                            name += presentationName[i] + " ";
                        }

                        fileName = name.Substring(0, name.Length - 1);
                        break;
                    }
                }
                String file_Path = files_path + "\\" + fileName;
                switch (sound_type_index)
                {
                    case 1: //background sound

                        main.chosen_backgorund_sound_file_LB.Text = fileName.Split(' ')[0];
                        main.background_sound_player = new audioPlayer(file_Path);
                        Mp3FileReader reader = new Mp3FileReader(file_Path + ".mp3");
                        TimeSpan duration = reader.TotalTime;
                        main.background_sound_player.file_Length = duration.TotalSeconds;
                        main.backgorung_sound_time_LB.Text = String.Format("00:00/{0}", time_format(duration));
                        main.chose_background_sound = true;
                        break;

                    case 2: //effect sound

                        main.chosen_effect_sound_file_LB.Text = fileName.Split(' ')[0];
                        main.effect_sound_player = new audioPlayer(file_Path);
                        Mp3FileReader reader2 = new Mp3FileReader(file_Path + ".mp3");
                        TimeSpan duration2 = reader2.TotalTime;
                        main.effect_sound_player.file_Length = duration2.TotalSeconds;
                        main.effect_sound_time_LB.Text = String.Format("00:00/{0}", time_format(duration2));
                        main.chose_effect_sound = true;
                        break;
                }
                this.Hide();
            }
            
        }
        
        private bool noSelectedOptions()
        {
            foreach(Control c in panel2.Controls)
            {
                if(c is RadioButton)
                {
                    if(((RadioButton)c).Checked)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
