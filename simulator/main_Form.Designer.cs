﻿namespace simulator
{
    partial class main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main_Form));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.scenario_Panel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.background_Sound_Panel = new System.Windows.Forms.Panel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.room_Panel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.effects_Panel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.scenario_Tab = new System.Windows.Forms.TabPage();
            this.headline_Label = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.sound_Bar_Panel = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.activationTab = new System.Windows.Forms.TabPage();
            this.room_map_panel = new System.Windows.Forms.Panel();
            this.smoke_2 = new simulator.Action_Button();
            this.smoke_3 = new simulator.Action_Button();
            this.light_2 = new simulator.Action_Button();
            this.smoke_4 = new simulator.Action_Button();
            this.flicker_3 = new simulator.Action_Button();
            this.light_3 = new simulator.Action_Button();
            this.flicker_4 = new simulator.Action_Button();
            this.light_1 = new simulator.Action_Button();
            this.flicker_1 = new simulator.Action_Button();
            this.smoke_1 = new simulator.Action_Button();
            this.chaka_1 = new simulator.Action_Button();
            this.flicker_2 = new simulator.Action_Button();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.chosen_backgorund_sound_file_LB = new System.Windows.Forms.Label();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.playing_background_file_LB = new System.Windows.Forms.Label();
            this.backgorung_sound_time_LB = new System.Windows.Forms.Label();
            this.background_sound_stop_BT = new System.Windows.Forms.PictureBox();
            this.background_sound_play_BT = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.chosen_effect_sound_file_LB = new System.Windows.Forms.Label();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.playing_effect_file_LB = new System.Windows.Forms.Label();
            this.effect_sound_time_LB = new System.Windows.Forms.Label();
            this.effect_sound_stop_BT = new System.Windows.Forms.PictureBox();
            this.effect_sound_play_BT = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.playing_scenario_name_LB = new System.Windows.Forms.Label();
            this.chosen_scenario_LB = new System.Windows.Forms.Label();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.scenario_timeLB = new System.Windows.Forms.Label();
            this.scenario_stop_BT = new System.Windows.Forms.PictureBox();
            this.scenario_play_BT = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.creationTab = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.scenarioTable = new System.Windows.Forms.DataGridView();
            this.element = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.durance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel7 = new System.Windows.Forms.Panel();
            this.maskedTextBox8 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox7 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox6 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox5 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox4 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox3 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this.type_CB1 = new System.Windows.Forms.ComboBox();
            this.type_CB2 = new System.Windows.Forms.ComboBox();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.type_CB4 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.type_CB3 = new System.Windows.Forms.ComboBox();
            this.identifier_CB4 = new System.Windows.Forms.ComboBox();
            this.identifier_CB3 = new System.Windows.Forms.ComboBox();
            this.identifier_CB2 = new System.Windows.Forms.ComboBox();
            this.identifier_CB1 = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.chackEndCB = new System.Windows.Forms.CheckBox();
            this.chakaStartCB = new System.Windows.Forms.CheckBox();
            this.scenarioNameTB = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.background_sound_Timer = new System.Windows.Forms.Timer(this.components);
            this.effect_sound_Timer = new System.Windows.Forms.Timer(this.components);
            this.scenario_Timer = new System.Windows.Forms.Timer(this.components);
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.Logo_Picture = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.scenarioTimer = new System.Windows.Forms.Timer(this.components);
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.scenario_Panel.SuspendLayout();
            this.background_Sound_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.room_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.effects_Panel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.sound_Bar_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.activationTab.SuspendLayout();
            this.room_map_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.background_sound_stop_BT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.background_sound_play_BT)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.effect_sound_stop_BT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.effect_sound_play_BT)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scenario_stop_BT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scenario_play_BT)).BeginInit();
            this.creationTab.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scenarioTable)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Logo_Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.scenario_Tab);
            this.tabControl1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.tabControl1.Location = new System.Drawing.Point(7, 74);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1090, 492);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.scenario_Panel);
            this.tabPage1.Controls.Add(this.background_Sound_Panel);
            this.tabPage1.Controls.Add(this.room_Panel);
            this.tabPage1.Controls.Add(this.effects_Panel);
            this.tabPage1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.tabPage1.Location = new System.Drawing.Point(4, 39);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1082, 449);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "הפעלה";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // scenario_Panel
            // 
            this.scenario_Panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scenario_Panel.Controls.Add(this.label5);
            this.scenario_Panel.Location = new System.Drawing.Point(5, 248);
            this.scenario_Panel.Name = "scenario_Panel";
            this.scenario_Panel.Size = new System.Drawing.Size(385, 200);
            this.scenario_Panel.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(381, 37);
            this.label5.TabIndex = 3;
            this.label5.Text = "הפעלת תרחיש";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // background_Sound_Panel
            // 
            this.background_Sound_Panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.background_Sound_Panel.Controls.Add(this.pictureBox7);
            this.background_Sound_Panel.Controls.Add(this.pictureBox4);
            this.background_Sound_Panel.Controls.Add(this.label2);
            this.background_Sound_Panel.Location = new System.Drawing.Point(200, 6);
            this.background_Sound_Panel.Name = "background_Sound_Panel";
            this.background_Sound_Panel.Size = new System.Drawing.Size(191, 237);
            this.background_Sound_Panel.TabIndex = 1;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::simulator.Properties.Resources.play_Button;
            this.pictureBox7.Location = new System.Drawing.Point(56, 185);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(47, 33);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 4;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::simulator.Properties.Resources.stop_Button;
            this.pictureBox4.Location = new System.Drawing.Point(3, 185);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(47, 33);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 1;
            this.pictureBox4.TabStop = false;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(187, 37);
            this.label2.TabIndex = 0;
            this.label2.Text = "סאונד רקע";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // room_Panel
            // 
            this.room_Panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.room_Panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.room_Panel.Controls.Add(this.pictureBox1);
            this.room_Panel.Location = new System.Drawing.Point(397, 6);
            this.room_Panel.Name = "room_Panel";
            this.room_Panel.Size = new System.Drawing.Size(679, 443);
            this.room_Panel.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::simulator.Properties.Resources.room_scetch;
            this.pictureBox1.Location = new System.Drawing.Point(44, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(610, 386);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // effects_Panel
            // 
            this.effects_Panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.effects_Panel.Controls.Add(this.panel1);
            this.effects_Panel.Controls.Add(this.label4);
            this.effects_Panel.Controls.Add(this.label3);
            this.effects_Panel.Location = new System.Drawing.Point(3, 6);
            this.effects_Panel.Name = "effects_Panel";
            this.effects_Panel.Size = new System.Drawing.Size(191, 237);
            this.effects_Panel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.pictureBox6);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-2, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(191, 237);
            this.panel1.TabIndex = 3;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::simulator.Properties.Resources.play_Button;
            this.pictureBox5.Location = new System.Drawing.Point(56, 185);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(47, 33);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::simulator.Properties.Resources.stop_Button;
            this.pictureBox6.Location = new System.Drawing.Point(3, 185);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(47, 33);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 1;
            this.pictureBox6.TabStop = false;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(187, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "סאונד רקע";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 37);
            this.label4.TabIndex = 2;
            this.label4.Text = "אפקטים";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(187, 37);
            this.label3.TabIndex = 1;
            this.label3.Text = "סאונד רקע";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scenario_Tab
            // 
            this.scenario_Tab.Location = new System.Drawing.Point(4, 39);
            this.scenario_Tab.Name = "scenario_Tab";
            this.scenario_Tab.Padding = new System.Windows.Forms.Padding(3);
            this.scenario_Tab.Size = new System.Drawing.Size(1082, 449);
            this.scenario_Tab.TabIndex = 1;
            this.scenario_Tab.Text = "יצירת תרחיש";
            this.scenario_Tab.UseVisualStyleBackColor = true;
            // 
            // headline_Label
            // 
            this.headline_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.headline_Label.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.headline_Label.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.headline_Label.Location = new System.Drawing.Point(0, 0);
            this.headline_Label.Name = "headline_Label";
            this.headline_Label.Size = new System.Drawing.Size(1109, 46);
            this.headline_Label.TabIndex = 2;
            this.headline_Label.Text = "\"סימולטור רפואה - \"בית בכפר";
            this.headline_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackBar1
            // 
            this.trackBar1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.trackBar1.Location = new System.Drawing.Point(40, 11);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.trackBar1.Size = new System.Drawing.Size(181, 45);
            this.trackBar1.TabIndex = 5;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
            // 
            // sound_Bar_Panel
            // 
            this.sound_Bar_Panel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sound_Bar_Panel.Controls.Add(this.pictureBox3);
            this.sound_Bar_Panel.Controls.Add(this.trackBar1);
            this.sound_Bar_Panel.Controls.Add(this.pictureBox2);
            this.sound_Bar_Panel.Location = new System.Drawing.Point(332, 593);
            this.sound_Bar_Panel.Name = "sound_Bar_Panel";
            this.sound_Bar_Panel.Size = new System.Drawing.Size(261, 49);
            this.sound_Bar_Panel.TabIndex = 8;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Image = global::simulator.Properties.Resources.decrease_Vol2;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 49);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Image = global::simulator.Properties.Resources.increase_Vol;
            this.pictureBox2.Location = new System.Drawing.Point(222, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 49);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tabControl2.Controls.Add(this.activationTab);
            this.tabControl2.Controls.Add(this.creationTab);
            this.tabControl2.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.tabControl2.Location = new System.Drawing.Point(4, 87);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl2.RightToLeftLayout = true;
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1238, 682);
            this.tabControl2.TabIndex = 3;
            this.tabControl2.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl2_Selecting);
            // 
            // activationTab
            // 
            this.activationTab.BackColor = System.Drawing.Color.White;
            this.activationTab.Controls.Add(this.room_map_panel);
            this.activationTab.Controls.Add(this.panel4);
            this.activationTab.Controls.Add(this.panel3);
            this.activationTab.Controls.Add(this.panel5);
            this.activationTab.Location = new System.Drawing.Point(4, 43);
            this.activationTab.Name = "activationTab";
            this.activationTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.activationTab.Size = new System.Drawing.Size(1230, 635);
            this.activationTab.TabIndex = 0;
            this.activationTab.Text = "הפעלה";
            // 
            // room_map_panel
            // 
            this.room_map_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.room_map_panel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.room_map_panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.room_map_panel.Controls.Add(this.smoke_2);
            this.room_map_panel.Controls.Add(this.smoke_3);
            this.room_map_panel.Controls.Add(this.light_2);
            this.room_map_panel.Controls.Add(this.smoke_4);
            this.room_map_panel.Controls.Add(this.flicker_3);
            this.room_map_panel.Controls.Add(this.light_3);
            this.room_map_panel.Controls.Add(this.flicker_4);
            this.room_map_panel.Controls.Add(this.light_1);
            this.room_map_panel.Controls.Add(this.flicker_1);
            this.room_map_panel.Controls.Add(this.smoke_1);
            this.room_map_panel.Controls.Add(this.chaka_1);
            this.room_map_panel.Controls.Add(this.flicker_2);
            this.room_map_panel.Controls.Add(this.pictureBox11);
            this.room_map_panel.Location = new System.Drawing.Point(401, 3);
            this.room_map_panel.Name = "room_map_panel";
            this.room_map_panel.Size = new System.Drawing.Size(825, 629);
            this.room_map_panel.TabIndex = 7;
            // 
            // smoke_2
            // 
            this.smoke_2.BackColor = System.Drawing.SystemColors.Control;
            this.smoke_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.smoke_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.smoke_2.Location = new System.Drawing.Point(245, 476);
            this.smoke_2.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.smoke_2.Name = "smoke_2";
            this.smoke_2.Size = new System.Drawing.Size(100, 82);
            this.smoke_2.TabIndex = 15;
            // 
            // smoke_3
            // 
            this.smoke_3.BackColor = System.Drawing.SystemColors.Control;
            this.smoke_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.smoke_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.smoke_3.Location = new System.Drawing.Point(703, 379);
            this.smoke_3.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.smoke_3.Name = "smoke_3";
            this.smoke_3.Size = new System.Drawing.Size(100, 82);
            this.smoke_3.TabIndex = 14;
            // 
            // light_2
            // 
            this.light_2.BackColor = System.Drawing.SystemColors.Control;
            this.light_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.light_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.light_2.Location = new System.Drawing.Point(704, 167);
            this.light_2.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.light_2.Name = "light_2";
            this.light_2.Size = new System.Drawing.Size(100, 82);
            this.light_2.TabIndex = 13;
            // 
            // smoke_4
            // 
            this.smoke_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smoke_4.BackColor = System.Drawing.SystemColors.Control;
            this.smoke_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.smoke_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.smoke_4.Location = new System.Drawing.Point(704, 18);
            this.smoke_4.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.smoke_4.Name = "smoke_4";
            this.smoke_4.Size = new System.Drawing.Size(99, 82);
            this.smoke_4.TabIndex = 12;
            // 
            // flicker_3
            // 
            this.flicker_3.BackColor = System.Drawing.SystemColors.Control;
            this.flicker_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.flicker_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flicker_3.Location = new System.Drawing.Point(506, 19);
            this.flicker_3.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.flicker_3.Name = "flicker_3";
            this.flicker_3.Size = new System.Drawing.Size(100, 82);
            this.flicker_3.TabIndex = 11;
            // 
            // light_3
            // 
            this.light_3.BackColor = System.Drawing.SystemColors.Control;
            this.light_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.light_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.light_3.Location = new System.Drawing.Point(245, 21);
            this.light_3.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.light_3.Name = "light_3";
            this.light_3.Size = new System.Drawing.Size(100, 82);
            this.light_3.TabIndex = 10;
            // 
            // flicker_4
            // 
            this.flicker_4.BackColor = System.Drawing.SystemColors.Control;
            this.flicker_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.flicker_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flicker_4.Location = new System.Drawing.Point(140, 164);
            this.flicker_4.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.flicker_4.Name = "flicker_4";
            this.flicker_4.Size = new System.Drawing.Size(100, 82);
            this.flicker_4.TabIndex = 9;
            // 
            // light_1
            // 
            this.light_1.BackColor = System.Drawing.SystemColors.Control;
            this.light_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.light_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.light_1.Location = new System.Drawing.Point(244, 380);
            this.light_1.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.light_1.Name = "light_1";
            this.light_1.Size = new System.Drawing.Size(100, 82);
            this.light_1.TabIndex = 8;
            // 
            // flicker_1
            // 
            this.flicker_1.BackColor = System.Drawing.SystemColors.Control;
            this.flicker_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.flicker_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flicker_1.Location = new System.Drawing.Point(245, 297);
            this.flicker_1.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.flicker_1.Name = "flicker_1";
            this.flicker_1.Size = new System.Drawing.Size(100, 82);
            this.flicker_1.TabIndex = 7;
            // 
            // smoke_1
            // 
            this.smoke_1.BackColor = System.Drawing.SystemColors.Control;
            this.smoke_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.smoke_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.smoke_1.Location = new System.Drawing.Point(245, 215);
            this.smoke_1.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.smoke_1.Name = "smoke_1";
            this.smoke_1.Size = new System.Drawing.Size(100, 82);
            this.smoke_1.TabIndex = 6;
            this.smoke_1.Load += new System.EventHandler(this.smoke_1_Load);
            // 
            // chaka_1
            // 
            this.chaka_1.BackColor = System.Drawing.SystemColors.Control;
            this.chaka_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chaka_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chaka_1.Location = new System.Drawing.Point(413, 476);
            this.chaka_1.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.chaka_1.Name = "chaka_1";
            this.chaka_1.Size = new System.Drawing.Size(100, 82);
            this.chaka_1.TabIndex = 5;
            // 
            // flicker_2
            // 
            this.flicker_2.BackColor = System.Drawing.Color.Transparent;
            this.flicker_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.flicker_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flicker_2.Location = new System.Drawing.Point(514, 476);
            this.flicker_2.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.flicker_2.Name = "flicker_2";
            this.flicker_2.Size = new System.Drawing.Size(100, 82);
            this.flicker_2.TabIndex = 4;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox11.Image = global::simulator.Properties.Resources.room_scetch;
            this.pictureBox11.Location = new System.Drawing.Point(34, 3);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(784, 623);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 3;
            this.pictureBox11.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.AutoSize = true;
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.chosen_backgorund_sound_file_LB);
            this.panel4.Controls.Add(this.pictureBox16);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.playing_background_file_LB);
            this.panel4.Controls.Add(this.backgorung_sound_time_LB);
            this.panel4.Controls.Add(this.background_sound_stop_BT);
            this.panel4.Controls.Add(this.background_sound_play_BT);
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(397, 221);
            this.panel4.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(393, 27);
            this.label12.TabIndex = 16;
            this.label12.Text = "סאונד רקע";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chosen_backgorund_sound_file_LB
            // 
            this.chosen_backgorund_sound_file_LB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chosen_backgorund_sound_file_LB.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.chosen_backgorund_sound_file_LB.Location = new System.Drawing.Point(6, 38);
            this.chosen_backgorund_sound_file_LB.Name = "chosen_backgorund_sound_file_LB";
            this.chosen_backgorund_sound_file_LB.Size = new System.Drawing.Size(321, 43);
            this.chosen_backgorund_sound_file_LB.TabIndex = 15;
            this.chosen_backgorund_sound_file_LB.Text = "בחר קובץ";
            this.chosen_backgorund_sound_file_LB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.chosen_backgorund_sound_file_LB.Click += new System.EventHandler(this.label11_Click);
            // 
            // pictureBox16
            // 
            this.pictureBox16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox16.Image = global::simulator.Properties.Resources.sound_File;
            this.pictureBox16.Location = new System.Drawing.Point(333, 34);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(52, 56);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 14;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.Click += new System.EventHandler(this.pictureBox16_Click);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label10.Location = new System.Drawing.Point(265, 109);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 27);
            this.label10.TabIndex = 13;
            this.label10.Text = "כעת מתנגן";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playing_background_file_LB
            // 
            this.playing_background_file_LB.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.playing_background_file_LB.Location = new System.Drawing.Point(-2, 111);
            this.playing_background_file_LB.Name = "playing_background_file_LB";
            this.playing_background_file_LB.Size = new System.Drawing.Size(275, 27);
            this.playing_background_file_LB.TabIndex = 12;
            this.playing_background_file_LB.Text = "קובץ לא הופעל";
            this.playing_background_file_LB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // backgorung_sound_time_LB
            // 
            this.backgorung_sound_time_LB.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.backgorung_sound_time_LB.Location = new System.Drawing.Point(224, 164);
            this.backgorung_sound_time_LB.Name = "backgorung_sound_time_LB";
            this.backgorung_sound_time_LB.Size = new System.Drawing.Size(151, 27);
            this.backgorung_sound_time_LB.TabIndex = 11;
            this.backgorung_sound_time_LB.Text = "00:00/00:00";
            this.backgorung_sound_time_LB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // background_sound_stop_BT
            // 
            this.background_sound_stop_BT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.background_sound_stop_BT.Image = global::simulator.Properties.Resources.stop_Button;
            this.background_sound_stop_BT.Location = new System.Drawing.Point(69, 153);
            this.background_sound_stop_BT.Name = "background_sound_stop_BT";
            this.background_sound_stop_BT.Size = new System.Drawing.Size(52, 57);
            this.background_sound_stop_BT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.background_sound_stop_BT.TabIndex = 10;
            this.background_sound_stop_BT.TabStop = false;
            this.background_sound_stop_BT.Click += new System.EventHandler(this.background_sound_stop_BT_Click);
            // 
            // background_sound_play_BT
            // 
            this.background_sound_play_BT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.background_sound_play_BT.Image = global::simulator.Properties.Resources.play_Button;
            this.background_sound_play_BT.Location = new System.Drawing.Point(144, 153);
            this.background_sound_play_BT.Name = "background_sound_play_BT";
            this.background_sound_play_BT.Size = new System.Drawing.Size(52, 57);
            this.background_sound_play_BT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.background_sound_play_BT.TabIndex = 9;
            this.background_sound_play_BT.TabStop = false;
            this.background_sound_play_BT.Click += new System.EventHandler(this.background_sound_play_BT_Click);
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.chosen_effect_sound_file_LB);
            this.panel3.Controls.Add(this.pictureBox17);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.playing_effect_file_LB);
            this.panel3.Controls.Add(this.effect_sound_time_LB);
            this.panel3.Controls.Add(this.effect_sound_stop_BT);
            this.panel3.Controls.Add(this.effect_sound_play_BT);
            this.panel3.Location = new System.Drawing.Point(3, 224);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(397, 213);
            this.panel3.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(393, 27);
            this.label13.TabIndex = 21;
            this.label13.Text = "אפקטים";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chosen_effect_sound_file_LB
            // 
            this.chosen_effect_sound_file_LB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chosen_effect_sound_file_LB.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.chosen_effect_sound_file_LB.Location = new System.Drawing.Point(7, 50);
            this.chosen_effect_sound_file_LB.Name = "chosen_effect_sound_file_LB";
            this.chosen_effect_sound_file_LB.Size = new System.Drawing.Size(320, 27);
            this.chosen_effect_sound_file_LB.TabIndex = 20;
            this.chosen_effect_sound_file_LB.Text = "בחר קובץ";
            this.chosen_effect_sound_file_LB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.chosen_effect_sound_file_LB.Click += new System.EventHandler(this.label14_Click);
            // 
            // pictureBox17
            // 
            this.pictureBox17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox17.Image = global::simulator.Properties.Resources.sound_File;
            this.pictureBox17.Location = new System.Drawing.Point(332, 30);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(52, 56);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 19;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Click += new System.EventHandler(this.pictureBox17_Click);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label15.Location = new System.Drawing.Point(278, 105);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(111, 27);
            this.label15.TabIndex = 18;
            this.label15.Text = "כעת מתנגן";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playing_effect_file_LB
            // 
            this.playing_effect_file_LB.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.playing_effect_file_LB.Location = new System.Drawing.Point(6, 106);
            this.playing_effect_file_LB.Name = "playing_effect_file_LB";
            this.playing_effect_file_LB.Size = new System.Drawing.Size(272, 27);
            this.playing_effect_file_LB.TabIndex = 17;
            this.playing_effect_file_LB.Text = "קובץ לא הופעל";
            this.playing_effect_file_LB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.playing_effect_file_LB.Click += new System.EventHandler(this.playing_effect_file_LB_Click);
            // 
            // effect_sound_time_LB
            // 
            this.effect_sound_time_LB.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.effect_sound_time_LB.Location = new System.Drawing.Point(226, 159);
            this.effect_sound_time_LB.Name = "effect_sound_time_LB";
            this.effect_sound_time_LB.Size = new System.Drawing.Size(137, 27);
            this.effect_sound_time_LB.TabIndex = 13;
            this.effect_sound_time_LB.Text = "00:00/00:00";
            this.effect_sound_time_LB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // effect_sound_stop_BT
            // 
            this.effect_sound_stop_BT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.effect_sound_stop_BT.Image = global::simulator.Properties.Resources.stop_Button;
            this.effect_sound_stop_BT.Location = new System.Drawing.Point(69, 144);
            this.effect_sound_stop_BT.Name = "effect_sound_stop_BT";
            this.effect_sound_stop_BT.Size = new System.Drawing.Size(52, 57);
            this.effect_sound_stop_BT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.effect_sound_stop_BT.TabIndex = 12;
            this.effect_sound_stop_BT.TabStop = false;
            this.effect_sound_stop_BT.Click += new System.EventHandler(this.effect_sound_stop_BT_Click);
            // 
            // effect_sound_play_BT
            // 
            this.effect_sound_play_BT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.effect_sound_play_BT.Image = global::simulator.Properties.Resources.play_Button;
            this.effect_sound_play_BT.Location = new System.Drawing.Point(144, 144);
            this.effect_sound_play_BT.Name = "effect_sound_play_BT";
            this.effect_sound_play_BT.Size = new System.Drawing.Size(52, 57);
            this.effect_sound_play_BT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.effect_sound_play_BT.TabIndex = 11;
            this.effect_sound_play_BT.TabStop = false;
            this.effect_sound_play_BT.Click += new System.EventHandler(this.effect_sound_play_BT_Click);
            // 
            // panel5
            // 
            this.panel5.AutoSize = true;
            this.panel5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.playing_scenario_name_LB);
            this.panel5.Controls.Add(this.chosen_scenario_LB);
            this.panel5.Controls.Add(this.pictureBox20);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.scenario_timeLB);
            this.panel5.Controls.Add(this.scenario_stop_BT);
            this.panel5.Controls.Add(this.scenario_play_BT);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Location = new System.Drawing.Point(3, 438);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(399, 200);
            this.panel5.TabIndex = 6;
            // 
            // playing_scenario_name_LB
            // 
            this.playing_scenario_name_LB.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.playing_scenario_name_LB.Location = new System.Drawing.Point(7, 90);
            this.playing_scenario_name_LB.Name = "playing_scenario_name_LB";
            this.playing_scenario_name_LB.Size = new System.Drawing.Size(266, 27);
            this.playing_scenario_name_LB.TabIndex = 24;
            this.playing_scenario_name_LB.Text = "קובץ לא הופעל";
            this.playing_scenario_name_LB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chosen_scenario_LB
            // 
            this.chosen_scenario_LB.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.chosen_scenario_LB.Location = new System.Drawing.Point(7, 41);
            this.chosen_scenario_LB.Name = "chosen_scenario_LB";
            this.chosen_scenario_LB.Size = new System.Drawing.Size(325, 27);
            this.chosen_scenario_LB.TabIndex = 23;
            this.chosen_scenario_LB.Text = "בחר קובץ";
            this.chosen_scenario_LB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox20.Image = global::simulator.Properties.Resources.sound_File;
            this.pictureBox20.Location = new System.Drawing.Point(338, 30);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(52, 56);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 22;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Click += new System.EventHandler(this.pictureBox20_Click);
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label20.Location = new System.Drawing.Point(256, 90);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(129, 27);
            this.label20.TabIndex = 21;
            this.label20.Text = "כעת מתנגן";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scenario_timeLB
            // 
            this.scenario_timeLB.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.scenario_timeLB.Location = new System.Drawing.Point(228, 145);
            this.scenario_timeLB.Name = "scenario_timeLB";
            this.scenario_timeLB.Size = new System.Drawing.Size(152, 27);
            this.scenario_timeLB.TabIndex = 20;
            this.scenario_timeLB.Text = "00:00/00:00";
            this.scenario_timeLB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.scenario_timeLB.Click += new System.EventHandler(this.scenario_timeLB_Click);
            // 
            // scenario_stop_BT
            // 
            this.scenario_stop_BT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.scenario_stop_BT.Image = global::simulator.Properties.Resources.stop_Button;
            this.scenario_stop_BT.Location = new System.Drawing.Point(69, 127);
            this.scenario_stop_BT.Name = "scenario_stop_BT";
            this.scenario_stop_BT.Size = new System.Drawing.Size(52, 57);
            this.scenario_stop_BT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.scenario_stop_BT.TabIndex = 19;
            this.scenario_stop_BT.TabStop = false;
            this.scenario_stop_BT.Click += new System.EventHandler(this.scenario_stop_BT_Click);
            // 
            // scenario_play_BT
            // 
            this.scenario_play_BT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.scenario_play_BT.Image = global::simulator.Properties.Resources.play_Button;
            this.scenario_play_BT.Location = new System.Drawing.Point(144, 127);
            this.scenario_play_BT.Name = "scenario_play_BT";
            this.scenario_play_BT.Size = new System.Drawing.Size(52, 57);
            this.scenario_play_BT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.scenario_play_BT.TabIndex = 18;
            this.scenario_play_BT.TabStop = false;
            this.scenario_play_BT.Click += new System.EventHandler(this.scenario_play_BT_Click);
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(395, 27);
            this.label17.TabIndex = 17;
            this.label17.Text = "הפעלת תרחיש";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // creationTab
            // 
            this.creationTab.BackColor = System.Drawing.Color.White;
            this.creationTab.Controls.Add(this.panel8);
            this.creationTab.Controls.Add(this.panel7);
            this.creationTab.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.creationTab.Location = new System.Drawing.Point(4, 43);
            this.creationTab.Name = "creationTab";
            this.creationTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.creationTab.Size = new System.Drawing.Size(1230, 635);
            this.creationTab.TabIndex = 1;
            this.creationTab.Text = "יצירת תרחיש";
            // 
            // panel8
            // 
            this.panel8.AutoScroll = true;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.button2);
            this.panel8.Controls.Add(this.scenarioTable);
            this.panel8.Location = new System.Drawing.Point(6, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(435, 682);
            this.panel8.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button2.Location = new System.Drawing.Point(19, 549);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(187, 68);
            this.button2.TabIndex = 27;
            this.button2.Text = "שמור תרחיש";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // scenarioTable
            // 
            this.scenarioTable.AllowUserToAddRows = false;
            this.scenarioTable.AllowUserToDeleteRows = false;
            this.scenarioTable.BackgroundColor = System.Drawing.Color.White;
            this.scenarioTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.scenarioTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.scenarioTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.element,
            this.startTime,
            this.durance});
            this.scenarioTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scenarioTable.Location = new System.Drawing.Point(0, 0);
            this.scenarioTable.Name = "scenarioTable";
            this.scenarioTable.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.scenarioTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.scenarioTable.RowHeadersVisible = false;
            this.scenarioTable.RowHeadersWidth = 51;
            this.scenarioTable.Size = new System.Drawing.Size(431, 678);
            this.scenarioTable.TabIndex = 0;
            // 
            // element
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.element.DefaultCellStyle = dataGridViewCellStyle1;
            this.element.HeaderText = "אמצעי";
            this.element.MinimumWidth = 6;
            this.element.Name = "element";
            this.element.ReadOnly = true;
            this.element.Width = 170;
            // 
            // startTime
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startTime.DefaultCellStyle = dataGridViewCellStyle2;
            this.startTime.HeaderText = "זמן התחלה";
            this.startTime.MinimumWidth = 6;
            this.startTime.Name = "startTime";
            this.startTime.ReadOnly = true;
            this.startTime.Width = 150;
            // 
            // durance
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.durance.DefaultCellStyle = dataGridViewCellStyle3;
            this.durance.HeaderText = "משך";
            this.durance.MinimumWidth = 6;
            this.durance.Name = "durance";
            this.durance.ReadOnly = true;
            this.durance.Width = 125;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.maskedTextBox8);
            this.panel7.Controls.Add(this.maskedTextBox7);
            this.panel7.Controls.Add(this.maskedTextBox6);
            this.panel7.Controls.Add(this.maskedTextBox5);
            this.panel7.Controls.Add(this.maskedTextBox4);
            this.panel7.Controls.Add(this.maskedTextBox3);
            this.panel7.Controls.Add(this.maskedTextBox2);
            this.panel7.Controls.Add(this.type_CB1);
            this.panel7.Controls.Add(this.type_CB2);
            this.panel7.Controls.Add(this.maskedTextBox1);
            this.panel7.Controls.Add(this.type_CB4);
            this.panel7.Controls.Add(this.button1);
            this.panel7.Controls.Add(this.type_CB3);
            this.panel7.Controls.Add(this.identifier_CB4);
            this.panel7.Controls.Add(this.identifier_CB3);
            this.panel7.Controls.Add(this.identifier_CB2);
            this.panel7.Controls.Add(this.identifier_CB1);
            this.panel7.Controls.Add(this.label28);
            this.panel7.Controls.Add(this.label27);
            this.panel7.Controls.Add(this.label26);
            this.panel7.Controls.Add(this.label25);
            this.panel7.Controls.Add(this.label24);
            this.panel7.Controls.Add(this.chackEndCB);
            this.panel7.Controls.Add(this.chakaStartCB);
            this.panel7.Controls.Add(this.scenarioNameTB);
            this.panel7.Controls.Add(this.label23);
            this.panel7.Controls.Add(this.label22);
            this.panel7.Location = new System.Drawing.Point(445, 3);
            this.panel7.Name = "panel7";
            this.panel7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.panel7.Size = new System.Drawing.Size(1426, 728);
            this.panel7.TabIndex = 0;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // maskedTextBox8
            // 
            this.maskedTextBox8.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.maskedTextBox8.Location = new System.Drawing.Point(4, 430);
            this.maskedTextBox8.Mask = "0000";
            this.maskedTextBox8.Name = "maskedTextBox8";
            this.maskedTextBox8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedTextBox8.Size = new System.Drawing.Size(164, 35);
            this.maskedTextBox8.TabIndex = 19;
            this.maskedTextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskedTextBox8.ValidatingType = typeof(int);
            this.maskedTextBox8.KeyUp += new System.Windows.Forms.KeyEventHandler(this.maskedTextBox8_KeyUp);
            // 
            // maskedTextBox7
            // 
            this.maskedTextBox7.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.maskedTextBox7.Location = new System.Drawing.Point(4, 385);
            this.maskedTextBox7.Mask = "0000";
            this.maskedTextBox7.Name = "maskedTextBox7";
            this.maskedTextBox7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedTextBox7.Size = new System.Drawing.Size(164, 35);
            this.maskedTextBox7.TabIndex = 15;
            this.maskedTextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskedTextBox7.ValidatingType = typeof(int);
            this.maskedTextBox7.KeyUp += new System.Windows.Forms.KeyEventHandler(this.maskedTextBox7_KeyUp);
            // 
            // maskedTextBox6
            // 
            this.maskedTextBox6.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.maskedTextBox6.Location = new System.Drawing.Point(4, 343);
            this.maskedTextBox6.Mask = "0000";
            this.maskedTextBox6.Name = "maskedTextBox6";
            this.maskedTextBox6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedTextBox6.Size = new System.Drawing.Size(164, 35);
            this.maskedTextBox6.TabIndex = 11;
            this.maskedTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskedTextBox6.ValidatingType = typeof(int);
            this.maskedTextBox6.KeyUp += new System.Windows.Forms.KeyEventHandler(this.maskedTextBox6_KeyUp);
            // 
            // maskedTextBox5
            // 
            this.maskedTextBox5.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.maskedTextBox5.Location = new System.Drawing.Point(4, 300);
            this.maskedTextBox5.Mask = "0000";
            this.maskedTextBox5.Name = "maskedTextBox5";
            this.maskedTextBox5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedTextBox5.Size = new System.Drawing.Size(164, 35);
            this.maskedTextBox5.TabIndex = 7;
            this.maskedTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskedTextBox5.ValidatingType = typeof(int);
            this.maskedTextBox5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.maskedTextBox5_KeyUp);
            // 
            // maskedTextBox4
            // 
            this.maskedTextBox4.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.maskedTextBox4.Location = new System.Drawing.Point(193, 431);
            this.maskedTextBox4.Mask = "00:00";
            this.maskedTextBox4.Name = "maskedTextBox4";
            this.maskedTextBox4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedTextBox4.Size = new System.Drawing.Size(168, 35);
            this.maskedTextBox4.TabIndex = 18;
            this.maskedTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskedTextBox4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.maskedTextBox4_KeyUp);
            // 
            // maskedTextBox3
            // 
            this.maskedTextBox3.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.maskedTextBox3.Location = new System.Drawing.Point(193, 389);
            this.maskedTextBox3.Mask = "00:00";
            this.maskedTextBox3.Name = "maskedTextBox3";
            this.maskedTextBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedTextBox3.Size = new System.Drawing.Size(168, 35);
            this.maskedTextBox3.TabIndex = 14;
            this.maskedTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskedTextBox3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.maskedTextBox3_KeyUp);
            // 
            // maskedTextBox2
            // 
            this.maskedTextBox2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.maskedTextBox2.Location = new System.Drawing.Point(193, 347);
            this.maskedTextBox2.Mask = "00:00";
            this.maskedTextBox2.Name = "maskedTextBox2";
            this.maskedTextBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedTextBox2.Size = new System.Drawing.Size(168, 35);
            this.maskedTextBox2.TabIndex = 10;
            this.maskedTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskedTextBox2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.maskedTextBox2_KeyUp);
            // 
            // type_CB1
            // 
            this.type_CB1.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.type_CB1.FormattingEnabled = true;
            this.type_CB1.Items.AddRange(new object[] {
            "סאונד אפקט",
            "סאונד רקע"});
            this.type_CB1.Location = new System.Drawing.Point(610, 301);
            this.type_CB1.Name = "type_CB1";
            this.type_CB1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.type_CB1.Size = new System.Drawing.Size(171, 35);
            this.type_CB1.TabIndex = 4;
            this.type_CB1.SelectedValueChanged += new System.EventHandler(this.type_CB1_SelectedValueChanged);
            // 
            // type_CB2
            // 
            this.type_CB2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.type_CB2.FormattingEnabled = true;
            this.type_CB2.Items.AddRange(new object[] {
            "סאונד אפקט",
            "סאונד רקע"});
            this.type_CB2.Location = new System.Drawing.Point(610, 345);
            this.type_CB2.Name = "type_CB2";
            this.type_CB2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.type_CB2.Size = new System.Drawing.Size(171, 35);
            this.type_CB2.TabIndex = 8;
            this.type_CB2.SelectedValueChanged += new System.EventHandler(this.type_CB2_SelectedValueChanged);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.maskedTextBox1.Location = new System.Drawing.Point(193, 304);
            this.maskedTextBox1.Mask = "00:00";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedTextBox1.Size = new System.Drawing.Size(168, 35);
            this.maskedTextBox1.TabIndex = 6;
            this.maskedTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskedTextBox1.Click += new System.EventHandler(this.maskedTextBox1_Click);
            this.maskedTextBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.maskedTextBox1_KeyUp);
            // 
            // type_CB4
            // 
            this.type_CB4.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.type_CB4.FormattingEnabled = true;
            this.type_CB4.Items.AddRange(new object[] {
            "סאונד אפקט",
            "סאונד רקע"});
            this.type_CB4.Location = new System.Drawing.Point(610, 432);
            this.type_CB4.Name = "type_CB4";
            this.type_CB4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.type_CB4.Size = new System.Drawing.Size(171, 35);
            this.type_CB4.TabIndex = 16;
            this.type_CB4.SelectedValueChanged += new System.EventHandler(this.type_CB4_SelectedValueChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button1.Location = new System.Drawing.Point(19, 550);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(189, 68);
            this.button1.TabIndex = 20;
            this.button1.Text = "הוסף לתרחיש";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // type_CB3
            // 
            this.type_CB3.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.type_CB3.FormattingEnabled = true;
            this.type_CB3.Items.AddRange(new object[] {
            "סאונד אפקט",
            "סאונד רקע"});
            this.type_CB3.Location = new System.Drawing.Point(610, 388);
            this.type_CB3.Name = "type_CB3";
            this.type_CB3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.type_CB3.Size = new System.Drawing.Size(171, 35);
            this.type_CB3.TabIndex = 12;
            this.type_CB3.SelectedIndexChanged += new System.EventHandler(this.type_CB3_SelectedIndexChanged);
            this.type_CB3.SelectedValueChanged += new System.EventHandler(this.type_CB3_SelectedValueChanged);
            // 
            // identifier_CB4
            // 
            this.identifier_CB4.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.identifier_CB4.FormattingEnabled = true;
            this.identifier_CB4.Location = new System.Drawing.Point(395, 431);
            this.identifier_CB4.Name = "identifier_CB4";
            this.identifier_CB4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.identifier_CB4.Size = new System.Drawing.Size(167, 35);
            this.identifier_CB4.TabIndex = 17;
            this.identifier_CB4.SelectedIndexChanged += new System.EventHandler(this.identifier_CB4_SelectedIndexChanged);
            // 
            // identifier_CB3
            // 
            this.identifier_CB3.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.identifier_CB3.FormattingEnabled = true;
            this.identifier_CB3.Location = new System.Drawing.Point(395, 389);
            this.identifier_CB3.Name = "identifier_CB3";
            this.identifier_CB3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.identifier_CB3.Size = new System.Drawing.Size(167, 35);
            this.identifier_CB3.TabIndex = 13;
            this.identifier_CB3.SelectedIndexChanged += new System.EventHandler(this.identifier_CB3_SelectedIndexChanged);
            // 
            // identifier_CB2
            // 
            this.identifier_CB2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.identifier_CB2.FormattingEnabled = true;
            this.identifier_CB2.Location = new System.Drawing.Point(395, 347);
            this.identifier_CB2.Name = "identifier_CB2";
            this.identifier_CB2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.identifier_CB2.Size = new System.Drawing.Size(167, 35);
            this.identifier_CB2.TabIndex = 9;
            this.identifier_CB2.SelectedIndexChanged += new System.EventHandler(this.identifier_CB2_SelectedIndexChanged);
            // 
            // identifier_CB1
            // 
            this.identifier_CB1.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.identifier_CB1.FormattingEnabled = true;
            this.identifier_CB1.Location = new System.Drawing.Point(395, 304);
            this.identifier_CB1.Name = "identifier_CB1";
            this.identifier_CB1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.identifier_CB1.Size = new System.Drawing.Size(167, 35);
            this.identifier_CB1.TabIndex = 5;
            this.identifier_CB1.SelectedIndexChanged += new System.EventHandler(this.identifier_CB1_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label28.Location = new System.Drawing.Point(31, 263);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(104, 33);
            this.label28.TabIndex = 9;
            this.label28.Text = "משך בשניות";
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label27.Location = new System.Drawing.Point(206, 263);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(135, 33);
            this.label27.TabIndex = 8;
            this.label27.Text = "זמן התחלה (mm:ss)";
            this.label27.Click += new System.EventHandler(this.label27_Click);
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label26.Location = new System.Drawing.Point(430, 263);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(84, 33);
            this.label26.TabIndex = 7;
            this.label26.Text = "מזהה";
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label25.Location = new System.Drawing.Point(652, 262);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 33);
            this.label25.TabIndex = 6;
            this.label25.Text = "סוג";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label24.Location = new System.Drawing.Point(578, 215);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(208, 36);
            this.label24.TabIndex = 5;
            this.label24.Text = "אמצעים לתרחיש";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // chackEndCB
            // 
            this.chackEndCB.AutoSize = true;
            this.chackEndCB.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.chackEndCB.Location = new System.Drawing.Point(354, 148);
            this.chackEndCB.Name = "chackEndCB";
            this.chackEndCB.Size = new System.Drawing.Size(424, 31);
            this.chackEndCB.TabIndex = 3;
            this.chackEndCB.Text = "כבה צ\'קלקה \"אימון פעיל\" בעת סוף תרחיש";
            this.chackEndCB.UseVisualStyleBackColor = true;
            this.chackEndCB.CheckedChanged += new System.EventHandler(this.chackEndCB_CheckedChanged);
            // 
            // chakaStartCB
            // 
            this.chakaStartCB.AutoSize = true;
            this.chakaStartCB.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.chakaStartCB.Location = new System.Drawing.Point(315, 110);
            this.chakaStartCB.Name = "chakaStartCB";
            this.chakaStartCB.Size = new System.Drawing.Size(464, 31);
            this.chakaStartCB.TabIndex = 2;
            this.chakaStartCB.Text = "הפעל צ\'קלקה \"אימון פעיל\" בעת תחילת תרחיש";
            this.chakaStartCB.UseVisualStyleBackColor = true;
            // 
            // scenarioNameTB
            // 
            this.scenarioNameTB.BackColor = System.Drawing.Color.White;
            this.scenarioNameTB.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.scenarioNameTB.Location = new System.Drawing.Point(387, 56);
            this.scenarioNameTB.Name = "scenarioNameTB";
            this.scenarioNameTB.Size = new System.Drawing.Size(258, 35);
            this.scenarioNameTB.TabIndex = 1;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label23.Location = new System.Drawing.Point(650, 56);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(122, 33);
            this.label23.TabIndex = 1;
            this.label23.Text = "שם תרחיש";
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Arial", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label22.Location = new System.Drawing.Point(557, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(217, 33);
            this.label22.TabIndex = 0;
            this.label22.Text = "פרטי תרחיש";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(70, 15);
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(135, 45);
            this.trackBar2.TabIndex = 6;
            this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar2.MouseCaptureChanged += new System.EventHandler(this.trackBar2_MouseCaptureChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.Controls.Add(this.pictureBox10);
            this.panel2.Controls.Add(this.pictureBox9);
            this.panel2.Controls.Add(this.trackBar2);
            this.panel2.Location = new System.Drawing.Point(333, 785);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(256, 53);
            this.panel2.TabIndex = 7;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::simulator.Properties.Resources.increase_Vol;
            this.pictureBox10.Location = new System.Drawing.Point(200, 5);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(47, 38);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 8;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Click += new System.EventHandler(this.pictureBox10_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::simulator.Properties.Resources.decrease_Vol2;
            this.pictureBox9.Location = new System.Drawing.Point(26, 3);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(46, 40);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 7;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Click += new System.EventHandler(this.pictureBox9_Click);
            // 
            // background_sound_Timer
            // 
            this.background_sound_Timer.Interval = 1000;
            this.background_sound_Timer.Tick += new System.EventHandler(this.background_sound_Timer_Tick);
            // 
            // effect_sound_Timer
            // 
            this.effect_sound_Timer.Interval = 1000;
            this.effect_sound_Timer.Tick += new System.EventHandler(this.effect_sound_Timer_Tick);
            // 
            // scenario_Timer
            // 
            this.scenario_Timer.Interval = 1000;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.Image = global::simulator.Properties.Resources.eltel_Logo;
            this.pictureBox8.Location = new System.Drawing.Point(3, 1);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(135, 72);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 2;
            this.pictureBox8.TabStop = false;
            // 
            // Logo_Picture
            // 
            this.Logo_Picture.BackColor = System.Drawing.Color.Transparent;
            this.Logo_Picture.Image = ((System.Drawing.Image)(resources.GetObject("Logo_Picture.Image")));
            this.Logo_Picture.Location = new System.Drawing.Point(0, 0);
            this.Logo_Picture.Name = "Logo_Picture";
            this.Logo_Picture.Size = new System.Drawing.Size(125, 68);
            this.Logo_Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Logo_Picture.TabIndex = 1;
            this.Logo_Picture.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(1148, 773);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(85, 75);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 8;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Click += new System.EventHandler(this.pictureBox12_Click);
            // 
            // scenarioTimer
            // 
            this.scenarioTimer.Interval = 1000;
            this.scenarioTimer.Tick += new System.EventHandler(this.scenarioTimer_Tick);
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button4.BackColor = System.Drawing.Color.Gainsboro;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button4.Location = new System.Drawing.Point(814, 785);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(235, 66);
            this.button4.TabIndex = 9;
            this.button4.Text = "אימון לא פעיל";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button5.BackColor = System.Drawing.Color.Gainsboro;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button5.Location = new System.Drawing.Point(12, 777);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(235, 66);
            this.button5.TabIndex = 10;
            this.button5.Text = "יציאה";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.AliceBlue;
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.button10);
            this.panel6.Controls.Add(this.button9);
            this.panel6.Controls.Add(this.button8);
            this.panel6.Controls.Add(this.button7);
            this.panel6.Controls.Add(this.button6);
            this.panel6.Controls.Add(this.button3);
            this.panel6.Enabled = false;
            this.panel6.Location = new System.Drawing.Point(595, 1);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(248, 120);
            this.panel6.TabIndex = 11;
            this.panel6.Visible = false;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label7.Location = new System.Drawing.Point(135, 84);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 37);
            this.label7.TabIndex = 6;
            this.label7.Text = "label7";
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button10.Location = new System.Drawing.Point(64, 3);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(28, 38);
            this.button10.TabIndex = 5;
            this.button10.Text = "^";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button9.Location = new System.Drawing.Point(64, 77);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(28, 32);
            this.button9.TabIndex = 4;
            this.button9.Text = "v";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button8.Location = new System.Drawing.Point(190, 7);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(40, 30);
            this.button8.TabIndex = 3;
            this.button8.Text = "+";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button7.Location = new System.Drawing.Point(190, 46);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(40, 30);
            this.button7.TabIndex = 2;
            this.button7.Text = "-";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button6.Location = new System.Drawing.Point(100, 46);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(43, 25);
            this.button6.TabIndex = 1;
            this.button6.Text = ">";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button3.Location = new System.Drawing.Point(16, 46);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(35, 25);
            this.button3.TabIndex = 0;
            this.button3.Text = "<";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_2);
            // 
            // main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(1247, 854);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.pictureBox8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "main_Form";
            this.Text = "סימולטור רפואה";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.main_Form_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.scenario_Panel.ResumeLayout(false);
            this.background_Sound_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.room_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.effects_Panel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.sound_Bar_Panel.ResumeLayout(false);
            this.sound_Bar_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.activationTab.ResumeLayout(false);
            this.activationTab.PerformLayout();
            this.room_map_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.background_sound_stop_BT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.background_sound_play_BT)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.effect_sound_stop_BT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.effect_sound_play_BT)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scenario_stop_BT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scenario_play_BT)).EndInit();
            this.creationTab.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scenarioTable)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Logo_Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage scenario_Tab;
        private System.Windows.Forms.PictureBox Logo_Picture;
        private System.Windows.Forms.Label headline_Label;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel sound_Bar_Panel;
        private System.Windows.Forms.Panel room_Panel;
        private System.Windows.Forms.Panel effects_Panel;
        private System.Windows.Forms.Panel scenario_Panel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel background_Sound_Panel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.TabPage activationTab;
        private System.Windows.Forms.TabPage creationTab;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Panel room_map_panel;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.DataGridView scenarioTable;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox type_CB4;
        private System.Windows.Forms.ComboBox type_CB3;
        private System.Windows.Forms.ComboBox type_CB2;
        private System.Windows.Forms.ComboBox type_CB1;
        private System.Windows.Forms.ComboBox identifier_CB4;
        private System.Windows.Forms.ComboBox identifier_CB3;
        private System.Windows.Forms.ComboBox identifier_CB2;
        private System.Windows.Forms.ComboBox identifier_CB1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox chackEndCB;
        private System.Windows.Forms.CheckBox chakaStartCB;
        private System.Windows.Forms.TextBox scenarioNameTB;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        public Action_Button flicker_2;
        public Action_Button chaka_1;
        public Action_Button light_1;
        public Action_Button flicker_1;
        public Action_Button smoke_1;
        public Action_Button light_3;
        public Action_Button flicker_4;
        public Action_Button light_2;
        public Action_Button smoke_4;
        public Action_Button flicker_3;
        public Action_Button smoke_3;
        public Action_Button smoke_2;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label playing_background_file_LB;
        public System.Windows.Forms.Label backgorung_sound_time_LB;
        public System.Windows.Forms.PictureBox background_sound_stop_BT;
        public System.Windows.Forms.PictureBox background_sound_play_BT;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label playing_effect_file_LB;
        public System.Windows.Forms.Label effect_sound_time_LB;
        public System.Windows.Forms.PictureBox effect_sound_stop_BT;
        public System.Windows.Forms.PictureBox effect_sound_play_BT;
        public System.Windows.Forms.Label playing_scenario_name_LB;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Label scenario_timeLB;
        public System.Windows.Forms.PictureBox scenario_stop_BT;
        public System.Windows.Forms.PictureBox scenario_play_BT;
        public System.Windows.Forms.Label chosen_backgorund_sound_file_LB;
        public System.Windows.Forms.PictureBox pictureBox16;
        public System.Windows.Forms.Label chosen_effect_sound_file_LB;
        public System.Windows.Forms.PictureBox pictureBox17;
        public System.Windows.Forms.Label chosen_scenario_LB;
        public System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.Timer background_sound_Timer;
        private System.Windows.Forms.Timer effect_sound_Timer;
        private System.Windows.Forms.Timer scenario_Timer;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox8;
        private System.Windows.Forms.MaskedTextBox maskedTextBox7;
        private System.Windows.Forms.MaskedTextBox maskedTextBox6;
        private System.Windows.Forms.MaskedTextBox maskedTextBox5;
        private System.Windows.Forms.MaskedTextBox maskedTextBox4;
        private System.Windows.Forms.MaskedTextBox maskedTextBox3;
        private System.Windows.Forms.MaskedTextBox maskedTextBox2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox12;
        public System.Windows.Forms.Timer scenarioTimer;
        public System.Windows.Forms.Button button4;
        public System.Windows.Forms.Button button5;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn element;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn durance;
    }
}

