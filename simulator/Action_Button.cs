﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace simulator
{
    /// <summary>
    /// Action button is a button thaht makes an action (turn on/off) in a specific element. 
    /// </summary>
    public partial class Action_Button : UserControl
    {
        Dictionary<String, KeyValuePair<String, Image>> types;
        List<ActionBT> buttonTypes;
        Color frame_color = Control.DefaultBackColor;
        public Boolean status = false;
        ActionBT action_button;
        public main_Form main;
        public Action_Button()
        {
            InitializeComponent();
            types = new Dictionary<string, KeyValuePair<String, Image>>
            {
                { "smoke", new KeyValuePair<String, Image>("עשן", Properties.Resources.smoke_BT1)},
                { "light", new KeyValuePair<String, Image>("תאורה", Properties.Resources.light_BT1)},
                { "flicker", new KeyValuePair<String, Image>("פליקר", Properties.Resources.flicker_BT1)},
                { "chaka", new KeyValuePair<String, Image>("צ'קלקה", Properties.Resources.chaka_BT)}
            };
            buttonTypes = new List<ActionBT>
            {
                { new ActionBT(){ID="smoke" , LabelText = "עשן", myImage = Properties.Resources.smoke_BT1} },
                { new ActionBT(){ID="light" , LabelText = "תאורה", myImage =  Properties.Resources.light_BT1} },
                { new ActionBT(){ID="flicker" , LabelText = "פליקר", myImage = Properties.Resources.flicker_BT1} },
                { new ActionBT(){ID="chaka" , LabelText = "", myImage = Properties.Resources.chaka_BT} }, //the label text here is empty because image already have title
            };
        }

        private void Action_Button_Load(object sender, EventArgs e)
        {
            try
            {
                String name = this.Name.Remove(this.Name.LastIndexOf('_'));
                action_button = buttonTypes.Where(q => q.ID == name).FirstOrDefault();
                if (action_button == null)
                    return;
                label1.Text = action_button.LabelText;
                if (action_button.ID != "chaka")
                {
                    label1.Text += this.Name.Substring(this.Name.LastIndexOf('_') + 1);
                }
                this.BackgroundImage = action_button.myImage;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {
            if (main.current_scenario == null)
            {
                clicked();
            }
            else if (!main.current_scenario.active)
            {
                clicked();
            }
        }
        private void Action_Button_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(frame_color, 5),
                           this.DisplayRectangle);
        }
        private void Action_Button_Click(object sender, EventArgs e)
        {
            if (main.current_scenario == null)
            {
                clicked();
            }
            else if (!main.current_scenario.active)
            {
                clicked();

            }
        }
        /// <summary>
        /// return true if click turn button on
        /// </summary>
        /// <returns> true if button turned on, false otherwise</returns>
        public bool clicked()
        {
            int index = main.adam_action.getIndexByName(Name);
            if (index == -1)
            {
                throw new Exception("element name error");
            }
            if (!status) // turn button on
            {
                
                this.Invalidate();
                if (this.Name.Contains("chaka"))
                {
                    main.button4.BackColor = frame_color;
                    new Thread(() =>
                    {

                        this.Invoke((MethodInvoker)delegate
                        {
                            WriteTextUnsafe("אימון פעיל");
                        });
                    }).Start();

                    main.button4.ForeColor = Color.White;
                }
             
                if (!main.adam_action.makeAction(index, 1))
                {
                    //main.Close();
                    noConnectionErrorcs errorPopUp = new noConnectionErrorcs(main);
                    main.Enabled = false;
                    errorPopUp.Show();
                    //System.Windows.Forms.Application.Exit();
                    //System.Environment.Exit(1);
                }
                else
                {
                    status = true;
                    frame_color = Color.Green;
                }
                return true;
            }
            else // turn button off
            {
                
                this.Invalidate();
                if (this.Name.Contains("chaka"))
                {
                    main.button4.BackColor = Color.Gainsboro;
                    new Thread(() =>
                    {
                        this.Invoke((MethodInvoker)delegate
                        {
                            WriteTextUnsafe("אימון לא פעיל");
                        });
                    }).Start();
                    main.button4.ForeColor = Color.Black;
                }
                if (!main.adam_action.makeAction(index, 0))
                {
                    // main.Close();
                    // System.Windows.Forms.Application.Exit();
                    //System.Environment.Exit(1);
                    noConnectionErrorcs errorPopUp = new noConnectionErrorcs(main);
                    main.Enabled = false;
                    errorPopUp.Show();
                }
                else
                {
                    status = false;
                    frame_color = Control.DefaultBackColor;
                }
                return false;
            }
        }


        private void WriteTextUnsafe(String txt)
        {
            main.button4.Text = txt;
        }
    }
}
