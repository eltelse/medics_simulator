﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace simulator
{
    /// <summary>
    /// audioPlayer class manages all audio palyers
    /// </summary>
    public class audioPlayer
    {
        public Timer timer;
        public Double file_Length;
        public String length = "00:00";
        public String file_Path;
        public Boolean is_palying = false;
        public String name;
        private double current_time = 0;
        public bool paused = false;
        WMPLib.WindowsMediaPlayer player;


        public audioPlayer(String filePath)
        {
            this.timer = new Timer();
            this.file_Path = filePath;
            Mp3FileReader reader = new Mp3FileReader(filePath+ ".mp3");
            TimeSpan duration = reader.TotalTime;
            length = utils.time_format(duration);
            FileStream fs = new FileStream(filePath + ".mp3",FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            this.name = Path.GetFileName(filePath + ".mp3");
            name = name.Substring(0, name.LastIndexOf('.'));
            player = new WMPLib.WindowsMediaPlayer();
            player.settings.volume = utils.currentVol;
        }

        /// <summary>
        /// start palying current mp3 file
        /// </summary>
        public void play()
        {
            player.URL = file_Path + ".mp3";
            player.controls.currentPosition = current_time;
            player.controls.play();
            is_palying = true;
            paused = false;
        }
        /// <summary>
        /// set currnet volume to given value 
        /// </summary>
        /// <param name="val"></param>
        public void setVol(int val)
        {
            player.settings.volume = val;
        }
        /// <summary>
        /// pause palying current mp3 file
        /// </summary>
        public void pause()
        {
            player.controls.pause();
            current_time = player.controls.currentPosition;
            paused = true;
            is_palying = false;
        }
        /// <summary>
        /// stop palying current mp3 file
        /// </summary>
        public void stop()
        {
            player.controls.stop();
            current_time = 0;
            paused = false;
            is_palying = false;
        }

       
    }
}
